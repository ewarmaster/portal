$(document).ready(
		function() {
			$("#register-modal").modal({
				backdrop : 'static',
				keyboard : false
			});

			$('#register-modal').on('shown.bs.modal', function() {
				$('#username').focus()
			});

			$("#register").submit(
					function(event) {
						let user = $('#register-modal').find(
								'input[name="user"]').val();
						let username = $('#register-modal').find(
								'input[name="username"]').val();
						let pass = $('#register-modal').find(
								'input[name="pass"]').val();
						let passagain = $('#register-modal').find(
								'input[name="pass-again"]').val();

						if (pass != passagain) {
							alert("As senhas informadas não são iguais");
							event.preventDefault();
						} else if (user == "") {
							alert("Informe o e-mail do usuário");
							event.preventDefault();
						} else if (username == "") {
							alert("Todos os campos são obrigatórios.");
							event.preventDefault();
						}
					});

		});