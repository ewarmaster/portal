$(document).ready(function() {
	$("#login-modal").modal({
		backdrop : 'static',
		keyboard : false
	});

	$('#login-modal').on('shown.bs.modal', function() {
		$('#user').focus()
	});

});