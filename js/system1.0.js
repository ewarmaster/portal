function getDivContent(_page,_div, _fields = {}){
	$.ajax({
		type:"POST", 
		data: _fields,
		url:"ajax/"+ _page +".php", 
		dataType:'html', 
		success:function(html){
			_div.html(html);
		}
	});
	}

$(document).ready(function () {
	let pagina = $("#menu_id").val();
	getDivContent(pagina,$('#main'));
	
	$(".nav-link").click(function (event) {
		if($(this).attr("href")!="?logout")	
			{ event.preventDefault();
			$(".collapse").collapse('toggle');
		      getDivContent($(this).attr("id"),$("#main"));
			}
	});
	
});