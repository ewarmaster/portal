<?php
session_start ();
require_once 'class/db.php';

$user = $_POST ["user"];
$pass = $_POST ["pass"];

$user_session = $_SESSION ["user_session"];

$result = 0;

if (isset ( $_GET ["logout"] )) {
	$user = "";
	$_SESSION ["user_session"] = "";
	$result = 3;
}

$menu = (isset ( $_GET ["conta"] )) ? "conta" : "inicial";
$menu = (isset ( $_GET ["reservatorio"] )) ? "reservatorio" : $menu;
$menu = (isset ( $_GET ["eventos"] )) ? "eventos" : $menu;

if ($_SESSION ["user_session"] != "")
	$result = 2;

if ($user != "") {
	$db = new db ();
	$db->query ( "SELECT * FROM subscriptions.hs_usuarios WHERE email='$user'" );
	$data = $db->fetch ();
	if ($db->numRows () == 1) {
		if ($data->password == md5 ( $pass )) {
			$_SESSION ["user_session"] = $data->nome;
			$_SESSION ["user_id"] = $data->id;
			$_SESSION ["user_priv"] = $data->perfil;
			$result = 2;
		} else {
			$result = 1;
		}
	}
}

if ($result != 2)
	header ( "location:index.php?result=" . $result );

?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Portal Hágua</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/portal.css">
<link rel="stylesheet" href="css/glyphi.css">
<link rel="icon" type="image/png" sizes="96x96" href="favicon.png">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="js/system1.0.js"></script>
</head>
<body>
	<input type="hidden" id="menu_id" value="<?=$menu;?>">
	<nav class="navbar bg-primary navbar-light dialog">
		<a class="navbar-brand" href="#">Hágua</a> <span class="navbar-text"><?=$_SESSION['user_session'];?></span>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">
				<li class="nav-item"><a class="nav-link" href="#" id="conta">Conta</a></li>
				<li class="nav-item"><a class="nav-link" href="#" id="eventos">Eventos</a></li>
				<li class="nav-item"><a class="nav-link" href="#" id="inicial">Dispositivos</a></li>
				<li class="nav-item"><a class="nav-link" href="#" id="reservatorio">Reservatório</a></li>

				<li class="nav-item"><a class="nav-link" href="?logout">Sair</a></li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid" id="main"></div>

</body>
</html>


