CREATE DATABASE  IF NOT EXISTS `membrana` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `membrana`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: membrana
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `condicoes`
--

DROP TABLE IF EXISTS `condicoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condicoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) DEFAULT NULL,
  `relativo` enum('SENSOR','HORA') DEFAULT 'SENSOR',
  `codigo` int(11) DEFAULT NULL,
  `condicao` enum('=','<','>','<>','>=','<=') DEFAULT NULL,
  `valor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor` (`sensor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condicoes`
--

LOCK TABLES `condicoes` WRITE;
/*!40000 ALTER TABLE `condicoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `condicoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivo`
--

DROP TABLE IF EXISTS `dispositivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) DEFAULT NULL,
  `slave` varchar(17) DEFAULT NULL,
  `nivel_controle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dispositivo` (`mac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivo`
--

LOCK TABLES `dispositivo` WRITE;
/*!40000 ALTER TABLE `dispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispositivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) DEFAULT NULL,
  `tipo_id` varchar(45) DEFAULT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `valor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor` (`sensor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fluxo`
--

DROP TABLE IF EXISTS `fluxo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fluxo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sensor_id` int(11) DEFAULT NULL,
  `fluxo` enum('E','S') DEFAULT 'S',
  `valor` float(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor` (`sensor_id`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fluxo`
--

LOCK TABLES `fluxo` WRITE;
/*!40000 ALTER TABLE `fluxo` DISABLE KEYS */;
/*!40000 ALTER TABLE `fluxo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leitura`
--

DROP TABLE IF EXISTS `leitura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leitura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sensor_id` int(11) DEFAULT NULL,
  `valor` float(11,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leitura`
--

LOCK TABLES `leitura` WRITE;
/*!40000 ALTER TABLE `leitura` DISABLE KEYS */;
/*!40000 ALTER TABLE `leitura` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `membrana`.`leitura_BEFORE_INSERT` BEFORE INSERT ON `leitura` FOR EACH ROW
BEGIN

SET @decimal  = NEW.valor-FLOOR(NEW.valor);
SET @degrau   = 1/4;
SET @faixa    = ROUND(@decimal/@degrau);
SET NEW.valor = FLOOR(NEW.valor)+@faixa*@degrau;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `membrana`.`leitura_AFTER_INSERT` AFTER INSERT ON `leitura` FOR EACH ROW
BEGIN

UPDATE membrana.sensor SET medida=NEW.valor WHERE id=NEW.sensor_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `niveis_controle`
--

DROP TABLE IF EXISTS `niveis_controle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `niveis_controle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(45) DEFAULT NULL,
  `ativar` float(3,2) DEFAULT NULL,
  `desativar` float(3,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `niveis_controle`
--

LOCK TABLES `niveis_controle` WRITE;
/*!40000 ALTER TABLE `niveis_controle` DISABLE KEYS */;
INSERT INTO `niveis_controle` VALUES (1,'moderado',0.50,0.80);
/*!40000 ALTER TABLE `niveis_controle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservatorio`
--

DROP TABLE IF EXISTS `reservatorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacao` varchar(45) DEFAULT NULL,
  `formato` enum('QUADRADO','CILINDRICO','CONICO') DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `largura` int(11) DEFAULT NULL,
  `comprimento` int(11) DEFAULT NULL,
  `capacidade` int(11) DEFAULT NULL,
  `razao_base` float(7,6) DEFAULT '1.000000',
  `lamina` float(11,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservatorio`
--

LOCK TABLES `reservatorio` WRITE;
/*!40000 ALTER TABLE `reservatorio` DISABLE KEYS */;
INSERT INTO `reservatorio` VALUES (2,'CAIXA_50L_PITCH','QUADRADO',33,29,60,42,1.000000,7.000000),(1,'CAIXA_500L','QUADRADO',75,100,100,500,1.000000,25.000000),(3,'CAIXA_10000L','CILINDRICO',328,200,200,10000,1.000000,10.000000),(7,'CAIXA ','QUADRADO',275,320,350,26880,1.000000,40.000000);
/*!40000 ALTER TABLE `reservatorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor`
--

DROP TABLE IF EXISTS `sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dispositivo_id` int(11) DEFAULT NULL,
  `porta` enum('A','B','C','D','E') DEFAULT 'D',
  `tipo` enum('SONICO','FLUXO','CONTATO','TEMPERATURA') DEFAULT NULL,
  `apelido` varchar(45) DEFAULT NULL,
  `reservatorio_id` int(11) DEFAULT NULL,
  `medida` float(11,6) DEFAULT '0.000000',
  `disponivel` int(11) DEFAULT '0',
  `meta_mensal` int(11) DEFAULT '0',
  `limiar_temperatura` int(11) NOT NULL DEFAULT '75',
  `atualizacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dispositivo` (`dispositivo_id`,`apelido`,`tipo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor`
--

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `sensor_BEFORE_UPDATE` BEFORE UPDATE ON `sensor` FOR EACH ROW BEGIN

SET @formato     = NULL;
SET @altura      = NULL;
SET @largura     = NULL;
SET @comprimento = NULL;
SET @razao_base  = NULL;
SET @lamina      = NULL;
SET @diferenca   = ABS(OLD.medida - NEW.medida);
SET @fluxo       = IF(OLD.medida>NEW.medida,'E','S');
SET @litros      = 0;

SELECT 
    formato, altura, largura, comprimento, razao_base, lamina
INTO @formato , @altura , @largura , @comprimento , @razao_base , @lamina FROM
    membrana.reservatorio
WHERE
    id = OLD.reservatorio_id;
  
IF (NEW.medida>@altura) THEN
   SET NEW.medida = @altura;
END IF;

SET @maximo = @altura - NEW.medida;
  
IF (OLD.tipo='SONICO') THEN

IF (@formato='QUADRADO') THEN

   SET @litros = ROUND(@largura*@comprimento*@diferenca/1000,3);
   SET NEW.disponivel = ROUND(@largura*@comprimento*@maximo/1000,3);
   IF (@diferenca!=0) THEN
   INSERT INTO membrana.fluxo SET sensor_id=OLD.id,
                                  fluxo=@fluxo,
                                  valor=@litros;
   END IF;

END IF;

IF (@formato='CILINDRICO') THEN

   SET @raio = @largura/2;
   SET @litros = ROUND((PI()*@diferenca*(@raio*@raio))/1000,3);
   SET NEW.disponivel = ROUND((PI()*@maximo*(@raio*@raio))/1000,3);
   IF (@diferenca!=0) THEN
   INSERT INTO membrana.fluxo SET sensor_id=OLD.id,
                                  fluxo=@fluxo,
                                  valor=@litros;
   END IF;
END IF;

IF (@formato='CONICO') THEN

   SET @pos    = 1-(@altura-(NEW.medida-@lamina))/@altura;
   SET @raio   = (@largura/2) - ((@largura/2)*(@razao_base*@pos));
   SET @litros = ROUND((PI()*@diferenca*(@raio*@raio))/1000,3);
   SET NEW.disponivel = ROUND((PI()*@maximo*(@raio*@raio))/1000,3);
   IF (@diferenca!=0) THEN
   INSERT INTO membrana.fluxo SET sensor_id=OLD.id,
                                  fluxo=@fluxo,
                                  valor=@litros;
	END IF;

END IF;

END IF;

IF (NEW.medida<>0) AND (OLD.tipo='FLUXO') THEN

   SET @litros = @medida;
   INSERT INTO membrana.fluxo SET sensor_id=OLD.id,
                                  fluxo='E',
                                  valor=@litros;

END IF;

IF (OLD.tipo='CONTATO') AND (@diferenca=1) THEN

    SET @estado = IF(NEW.medida=1,'LIGADA','DESLIGADA');
    INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                    tipo_id=1,
                                    valor=@estado;

END IF;

IF (OLD.tipo='TEMPERATURA') THEN

    SET @tempo = 0;
	SELECT membrana.sf_lastEvent(7, OLD.id) INTO @tempo;
	IF (NEW.medida>=OLD.limiar_temperatura) AND ((@tempo>15) OR (@tempo IS NULL)) THEN
      INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                        tipo_id=7,
                                        valor=CONCAT(ROUND(NEW.medida,2),' ºC');
   END IF;

END IF;

IF (OLD.tipo='SONICO') THEN

   SET @flow  = NULL;
   SET @tempo = NULL;
SELECT membrana.sf_newFlow(OLD.id), membrana.sf_lastEvent(2, OLD.id) INTO @flow , @tempo;
   
   IF (@flow=0) AND ((@tempo>720) OR (@tempo IS NULL)) THEN
      INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                        tipo_id=2;
   END IF;
    
   SET @available = NULL;
   SET @tempo     = NULL;
   
SELECT 
    membrana.sf_available(OLD.id, NEW.disponivel),
    membrana.sf_lastEvent(5, OLD.id)
INTO @available , @tempo;
   
   IF (@available<=50) AND ((@tempo>60) OR (@tempo IS NULL)) THEN
      INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                        tipo_id=5,
                                        valor=CONCAT(round(@available,2), ' %');
   END IF; 
   
   IF (@fluxo='S') THEN
   
   SET @tempo     = NULL;
   
SELECT 
    membrana.sf_available(OLD.id, @litros),
    membrana.sf_lastEvent(4, OLD.id)
INTO @available , @tempo;
   
   IF (@available>=10) AND ((@tempo>60) OR (@tempo IS NULL)) THEN
      INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                        tipo_id=4,
                                        valor=CONCAT(ROUND(@litros,0), 'L');
   END IF;
   
   IF (OLD.meta_mensal>0) THEN
   
   SET @mensal = NULL;
   SET @tempo  = NULL;
          
SELECT membrana.sf_lastEvent(3, OLD.id),membrana.sf_waterUsed(OLD.id) INTO @tempo, @mensal;
   
   IF (@mensal>=OLD.meta_mensal) AND ((@tempo=0) OR (@tempo IS NULL)) THEN

      INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                        tipo_id=3,
                                        valor=CONCAT(@mensal, 'L');
   
   END IF;
   
   END IF;
   
   IF (NEW.disponivel>@capacidade) THEN
   
      SET @tempo = NULL;
      SELECT membrana.sf_lastEvent(6, OLD.id) INTO @tempo;
      
      IF (@tempo>60) OR (@tempo IS NULL) THEN
      INSERT INTO membrana.evento SET sensor_id=OLD.id,
                                        tipo_id=6,
                                        valor=CONCAT(ROUND(NEW.disponivel,0), 'L');
   END IF;
      
   END IF;
   
   END IF;

END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tipo_evento`
--

DROP TABLE IF EXISTS `tipo_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL,
  `severidade` enum('A','M','U') DEFAULT NULL,
  `sigla` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_evento`
--

LOCK TABLES `tipo_evento` WRITE;
/*!40000 ALTER TABLE `tipo_evento` DISABLE KEYS */;
INSERT INTO `tipo_evento` VALUES (1,'MUDANÇA DE ESTADO DA BOMBA','A','BOMB'),(2,'FORNECIMENTO INTERROMPIDO','M','FINT'),(3,'META DE CONSUMO ATINGIDA','A','META'),(4,'PICO DE CONSUMO','A','PICO'),(5,'ALERTA DE NIVEL CRITICO','U','NCRI'),(6,'ALERTA DE TRANSBORDO','U','ATRA'),(7,'ALERTA TEMPERATURA ALTA','U','ATAL');
/*!40000 ALTER TABLE `tipo_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'membrana'
--
/*!50003 DROP FUNCTION IF EXISTS `sf_available` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_available`(_sensor_id INT, _atual INT) RETURNS float(6,2)
BEGIN

SET @maximo = NULL;

SELECT capacidade INTO @maximo FROM membrana.reservatorio r LEFT JOIN membrana.sensor s ON (r.id=s.reservatorio_id)
WHERE s.id=_sensor_id;

RETURN ROUND(100*_atual/@maximo,2);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_flow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_flow`(_sensor_id INT) RETURNS tinyint(1)
BEGIN

SET @diferenca = NULL;
SET @minutos   = NULL;
SET @atual     = NULL;
SET @lamina    = NULL;

SELECT (v1-v2) ,ROUND(time_to_sec(timediff(d2,d1))/60,0),v2  INTO @diferenca,@minutos,@atual FROM
(SELECT sensor_id,valor as v1,data as d1 FROM membrana.leitura WHERE sensor_id=_sensor_id ORDER BY id DESC Limit 1,1) A
LEFT JOIN
(SELECT sensor_id,valor as v2,data as d2 FROM membrana.leitura WHERE sensor_id=_sensor_id ORDER BY id DESC Limit 0,1) B
ON (A.sensor_id=B.sensor_id);

SELECT lamina INTO @lamina FROM membrana.reservatorio r LEFT JOIN membrana.sensor s ON (r.id=s.reservatorio_id)
WHERE s.id=_sensor_id;

SET @flow = IF(@diferenca>0,1,IF(@minutos>=30,0,1));
SET @flow = IF(@lamina>=@atual,1,@flow);

RETURN @flow;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_jsonData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_jsonData`(`_id` INT) RETURNS text CHARSET latin1
BEGIN

SET @json = "{ ";
SET @aspas = char(34);

SET @disponivel     = NULL;
SET @percentual     = NULL;
SET @mensal         = NULL;
SET @diario         = NULL;
SET @horario        = NULL;
SET @consumo_labels = NULL;
SET @consumo_data   = NULL;
SET @eventos        = NULL;

SELECT disponivel INTO @disponivel FROM membrana.sensor WHERE id=_id;
SELECT ROUND(membrana.sf_available(_id,@disponivel),0) INTO @percentual;
SELECT ROUND(SUM(valor),0) INTO @mensal  FROM membrana.fluxo WHERE sensor_id=_id and fluxo='S' and data>=date_sub(now(), INTERVAL 30 DAY); 
SELECT ROUND(SUM(valor),0) INTO @diario  FROM membrana.fluxo WHERE sensor_id=_id and fluxo='S' and date(data)=curdate(); 
SELECT ROUND(SUM(valor),0) INTO @horario FROM membrana.fluxo WHERE sensor_id=_id and fluxo='S' and data>=date_sub(now(), INTERVAL 1 HOUR); 
SELECT GROUP_CONCAT('"',RIGHT(hora,2),'"'), GROUP_CONCAT(valor) INTO @consumo_labels, @consumo_data 
       FROM  (SELECT DATE_FORMAT(data,'%Y-%m-%d %H') as hora,ROUND(SUM(valor)) as valor FROM membrana.fluxo
             WHERE data>=date_sub(now(),INTERVAL 24 HOUR) and sensor_id=_id and fluxo='S'
             GROUP BY DATE_FORMAT(data,'%Y-%m-%d %H')) a;

SET @json = CONCAT(@json,@aspas,"disponivel",@aspas," : ",IFNULL(@disponivel,0), ' ,');
SET @json = CONCAT(@json,@aspas,"percentual",@aspas," : ",IFNULL(@percentual,0), ' ,');
SET @json = CONCAT(@json,@aspas,"mensal"    ,@aspas," : ",IFNULL(@mensal,0), ' ,');
SET @json = CONCAT(@json,@aspas,"diario"    ,@aspas," : ",IFNULL(@diario,0), ' ,');
SET @json = CONCAT(@json,@aspas,"horario"   ,@aspas," : ",IFNULL(@horario,0),' ,');
SET @json = CONCAT(@json,@aspas,"consumo"   ,@aspas," : {");
SET @json = CONCAT(@json,@aspas,"labels"    ,@aspas," : [ ",IFNULL(@consumo_labels,"")," ],");
SET @json = CONCAT(@json,@aspas,"data"      ,@aspas," : [ ",IFNULL(@consumo_data,"")," ] },");
SET @json = CONCAT(@json,@aspas,"eventos"   ,@aspas," : [ {",IFNULL(@eventos,""), " } ]");

SET @json = CONCAT(@json," }");

RETURN @json;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_lastEvent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_lastEvent`(_event INT,_sensor INT) RETURNS int(11)
BEGIN

SET @tempo  = NULL;
SET @mensal = NULL;

SELECT 
    ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), data)) / 60,
            0),
    IF(YEAR(data) = YEAR(CURDATE())
            AND MONTH(data) = MONTH(CURDATE()),
        1,
        0)
INTO @tempo , @mensal FROM
    membrana.evento
WHERE
    sensor_id = _sensor AND tipo_id = _event
ORDER BY id DESC
LIMIT 0 , 1;

IF (_event=3) THEN
   SET @tempo = @mensal;
END IF;

RETURN @tempo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_newFlow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_newFlow`(`_sensor_id` INT) RETURNS tinyint(4)
BEGIN

SET @lamina   = NULL;
SET @newFluxo = NULL;
SET @minimo   = NULL;

SELECT SUM(IF(fluxo='E',valor,(-1)*valor)) INTO @newFluxo FROM membrana.fluxo WHERE sensor_id=_sensor_id and data>=date_sub(now(), INTERVAL 1 HOUR)
GROUP BY sensor_id;

SELECT min(valor) into @minimo FROM membrana.leitura WHERE sensor_id=_sensor_id and data>=date_sub(now(), INTERVAL 1 HOUR)
GROUP BY sensor_id;

SELECT lamina INTO @lamina FROM membrana.reservatorio r LEFT JOIN membrana.sensor s ON (r.id=s.reservatorio_id)
WHERE s.id=_sensor_id;

SET @flow = IF(@newFluxo>=0,1,0);
SET @flow = IF(@lamina>=@minimo,1,@flow);

RETURN @flow;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_PercToDist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_PercToDist`(_dispositivo INT, _perc FLOAT(3,2)) RETURNS int(11)
BEGIN

SET @valor       = 0;
SET @formato     = NULL;
SET @altura      = NULL;
SET @largura     = NULL;
SET @comprimento = NULL;
SET @razao_base  = NULL;
SET @lamina      = NULL;
SET @capacidade  = NULL;

SELECT 
    formato, altura, largura, comprimento, razao_base, lamina, capacidade
INTO @formato , @altura , @largura , @comprimento , @razao_base , @lamina,@capacidade FROM
    membrana.reservatorio r, membrana.sensor s
WHERE
   r.id=s.reservatorio_id AND
   s.dispositivo_id=_dispositivo AND
   s.tipo='SONICO';
    
SET @litros = @capacidade * _perc;

IF (@formato='QUADRADO') THEN
   SET @valor = @altura - ROUND((1000 * @litros)/(@largura*@comprimento),0);
END IF;

IF (@formato='CILINDRICO') THEN
   SET @valor = @altura - ROUND((1000 * @litros)/((@largura/2)*(@largura/2)*pi()),0);
END IF;

RETURN @valor;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_testCondition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_testCondition`(_id INT) RETURNS tinyint(1)
BEGIN

SET @relativo = NULL;
SET @codigo   = NULL;
SET @condicao = NULL;
SET @valor    = NULL;
SET @ativa    = 0;
SET @medida   = NULL;

SELECT 
    relativo, codigo, condicao, valor
INTO @relativo , @codigo , @condicao , @valor FROM
    membrana.condicoes
WHERE
    id = _id;

IF (@relativo='SENSOR') THEN
    
SELECT 
    medida
INTO @medida FROM
    membrana.sensor
WHERE
    id = @codigo;

END IF;

IF (@relativo='HORA') THEN

   SET @medida = TIME(NOW());

END IF;

CASE @condicao
    WHEN '<'  THEN SET @ativa = IF(@medida<@valor,1,0);
    WHEN '>'  THEN SET @ativa = IF(@medida>@valor,1,0);
    WHEN '='  THEN SET @ativa = IF(@medida=@valor,1,0);
    WHEN '<>' THEN SET @ativa = IF(@medida<>@valor,1,0);
    WHEN '<=' THEN SET @ativa = IF(@medida<=@valor,1,0);
    WHEN '>=' THEN SET @ativa = IF(@medida>=@valor,1,0);
    ELSE
       SET @ativa = 0;
    END CASE;

RETURN @ativa;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_waterUsed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_waterUsed`(_sensor_id INT) RETURNS int(11)
BEGIN

SET @mensal = NULL;

SELECT 
    ROUND(SUM(valor),0)
INTO @mensal FROM
    membrana.fluxo
WHERE
    sensor_id = _sensor_id AND fluxo = 'S'
        AND MONTH(data) = MONTH(CURDATE())
        AND YEAR(data) = YEAR(CURDATE());

RETURN @mensal;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_checkCondition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_checkCondition`(_sensor_id INT)
BEGIN

SELECT IF(condicoes=verdadeiras,1,0) as ativar FROM
(SELECT SUM(1) as condicoes, SUM(membrana.sf_testCondition(id)) as verdadeiras
FROM membrana.condicoes WHERE sensor_id = _sensor_id) A;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_jsonData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jsonData`(IN `_id` INT)
    NO SQL
BEGIN

SET @json = "{ ";
SET @aspas = char(34);

SET @disponivel     = NULL;
SET @percentual     = NULL;
SET @mensal         = NULL;
SET @diario         = NULL;
SET @horario        = NULL;
SET @consumo_labels = NULL;
SET @consumo_data   = NULL;
SET @eventos        = NULL;

SET @json = CONCAT(@json,@aspas,"disponivel",@aspas," : ",IFNULL(@disponivel,0), ' ,');
SET @json = CONCAT(@json,@aspas,"percentual",@aspas," : ",IFNULL(@percentual,0), ' ,');
SET @json = CONCAT(@json,@aspas,"mensal"    ,@aspas," : ",IFNULL(@mensal,0), ' ,');
SET @json = CONCAT(@json,@aspas,"diario"    ,@aspas," : ",IFNULL(@diario,0), ' ,');
SET @json = CONCAT(@json,@aspas,"horario"   ,@aspas," : ",IFNULL(@horario,0),' ,');
SET @json = CONCAT(@json,@aspas,"consumo"   ,@aspas," : {");
SET @json = CONCAT(@json,@aspas,"labels"    ,@aspas," : [ ",IFNULL(@consumo_labels,"")," ],");
SET @json = CONCAT(@json,@aspas,"data"      ,@aspas," : [ ",IFNULL(@consumo_data,"")," ] },");
SET @json = CONCAT(@json,@aspas,"eventos"   ,@aspas," : [ {",IFNULL(@eventos,""), " } ]");

SET @json = CONCAT(@json," }");

SELECT @json as json;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `whoIAm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `whoIAm`(IN `_mac` VARCHAR(20))
    NO SQL
BEGIN

 

SET @id       = NULL;

SET @controle = NULL;

SET @funcao   = NULL;

 

SELECT id,nivel_controle_id,IF(`slave`<>'','M','S') INTO @id,@controle,@funcao FROM membrana.dispositivo WHERE mac=_mac;

IF (@id IS NULL) THEN

    SELECT 0 as dispositivo_id;

ELSE

SET @ativa    = 0;

SET @desativa = 0;

 

SELECT membrana.sf_PercToDist(d.id,n.ativar),

       membrana.sf_PercToDist(d.id,n.desativar)

  INTO @ativa, @desativa

  FROM membrana.dispositivo d, niveis_controle n

WHERE mac=_mac AND n.id=nivel_controle_id;

 

SELECT mac,dispositivo_id as did,

IF(`slave`<>'',`slave`,'') as 'slave',

#SUM(IF(porta='A',id,0)) as iA, SUM(IF(porta='A',tipo,0)) as tA,

#SUM(IF(porta='B',id,0)) as iB, SUM(IF(porta='B',tipo,0)) as tB,

#SUM(IF(porta='C',id,0)) as iC, #SUM(IF(porta='C',IF(tipo=4,limiar_temperatura,tipo),0)) as tC,

SUM(IF(porta='D',id,0)) as iD, SUM(IF(porta='D',tipo,0)) as tD,

SUM(IF(porta='E',id,0)) as iE, SUM(IF(porta='E',tipo,0)) as tE,

@ativa as bot,

@desativa as top

FROM (

SELECT mac,dispositivo_id,s.id,s.porta,IF(s.tipo='FLUXO',1,IF(s.tipo='SONICO',2,IF(s.tipo='CONTATO',3,4))) as tipo,`slave`,s.limiar_temperatura

  FROM membrana.dispositivo d LEFT JOIN membrana.sensor s ON (s.dispositivo_id=d.id)

WHERE mac=_mac) A

GROUP BY dispositivo_id, slave;

END IF;

 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-26 20:27:48
CREATE DATABASE  IF NOT EXISTS `subscriptions` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `subscriptions`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: subscriptions
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hs_produtos`
--

DROP TABLE IF EXISTS `hs_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hs_produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `uid` varchar(8) DEFAULT NULL,
  `fabricacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mac` (`mac`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_produtos`
--

LOCK TABLES `hs_produtos` WRITE;
/*!40000 ALTER TABLE `hs_produtos` DISABLE KEYS */;
INSERT INTO `hs_produtos` VALUES (2,'01',1,'AB','2019-02-21 23:28:36'),(3,'B4:E6:2D:37:71:E6',1,'B4:E6','2019-02-22 00:01:28');
/*!40000 ALTER TABLE `hs_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hs_produtos_tipo`
--

DROP TABLE IF EXISTS `hs_produtos_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hs_produtos_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_produtos_tipo`
--

LOCK TABLES `hs_produtos_tipo` WRITE;
/*!40000 ALTER TABLE `hs_produtos_tipo` DISABLE KEYS */;
INSERT INTO `hs_produtos_tipo` VALUES (1,'Haqua Monitor Sonico'),(2,'Haqua Monitor Fluxo');
/*!40000 ALTER TABLE `hs_produtos_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hs_system`
--

DROP TABLE IF EXISTS `hs_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hs_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_system`
--

LOCK TABLES `hs_system` WRITE;
/*!40000 ALTER TABLE `hs_system` DISABLE KEYS */;
INSERT INTO `hs_system` VALUES (1,'SQL de criação do banco de dados','\n\nCREATE DATABASE  IF NOT EXISTS `{_db_name_}` /*!40100 DEFAULT CHARACTER SET latin1 */;\nUSE `{_db_name_}`;\n-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)\n--\n-- Host: localhost    Database: {_db_name_}\n-- ------------------------------------------------------\n-- Server version	5.7.25-0ubuntu0.16.04.2\n\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\n/*!40101 SET NAMES utf8 */;\n/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;\n/*!40103 SET TIME_ZONE=\'+00:00\' */;\n/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;\n/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=\'NO_AUTO_VALUE_ON_ZERO\' */;\n/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;\n\n--\n-- Table structure for table `condicoes`\n--\n\nDROP TABLE IF EXISTS `condicoes`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `condicoes` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `sensor_id` int(11) DEFAULT NULL,\n  `relativo` enum(\'SENSOR\',\'HORA\') DEFAULT \'SENSOR\',\n  `codigo` int(11) DEFAULT NULL,\n  `condicao` enum(\'=\',\'<\',\'>\',\'<>\',\'>=\',\'<=\') DEFAULT NULL,\n  `valor` varchar(45) DEFAULT NULL,\n  PRIMARY KEY (`id`),\n  KEY `sensor` (`sensor_id`)\n) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `condicoes`\n--\n\nLOCK TABLES `condicoes` WRITE;\n/*!40000 ALTER TABLE `condicoes` DISABLE KEYS */;\n/*!40000 ALTER TABLE `condicoes` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Table structure for table `dispositivo`\n--\n\nDROP TABLE IF EXISTS `dispositivo`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `dispositivo` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `mac` varchar(17) DEFAULT NULL,\n  `slave` varchar(17) DEFAULT NULL,\n  `nivel_controle_id` int(11) DEFAULT NULL,\n  PRIMARY KEY (`id`),\n  KEY `dispositivo` (`mac`)\n) ENGINE=MyISAM DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `dispositivo`\n--\n\nLOCK TABLES `dispositivo` WRITE;\n/*!40000 ALTER TABLE `dispositivo` DISABLE KEYS */;\n/*!40000 ALTER TABLE `dispositivo` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Table structure for table `evento`\n--\n\nDROP TABLE IF EXISTS `evento`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `evento` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `sensor_id` int(11) DEFAULT NULL,\n  `tipo_id` varchar(45) DEFAULT NULL,\n  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,\n  `valor` varchar(45) DEFAULT NULL,\n  PRIMARY KEY (`id`),\n  KEY `sensor` (`sensor_id`)\n) ENGINE=MyISAM DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `evento`\n--\n\nLOCK TABLES `evento` WRITE;\n/*!40000 ALTER TABLE `evento` DISABLE KEYS */;\n/*!40000 ALTER TABLE `evento` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Table structure for table `fluxo`\n--\n\nDROP TABLE IF EXISTS `fluxo`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `fluxo` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,\n  `sensor_id` int(11) DEFAULT NULL,\n  `fluxo` enum(\'E\',\'S\') DEFAULT \'S\',\n  `valor` float(10,4) DEFAULT NULL,\n  PRIMARY KEY (`id`),\n  KEY `sensor` (`sensor_id`),\n  KEY `data` (`data`)\n) ENGINE=InnoDB DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `fluxo`\n--\n\nLOCK TABLES `fluxo` WRITE;\n/*!40000 ALTER TABLE `fluxo` DISABLE KEYS */;\n/*!40000 ALTER TABLE `fluxo` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Table structure for table `leitura`\n--\n\nDROP TABLE IF EXISTS `leitura`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `leitura` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,\n  `sensor_id` int(11) DEFAULT NULL,\n  `valor` float(11,6) DEFAULT NULL,\n  PRIMARY KEY (`id`)\n) ENGINE=MyISAM DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `leitura`\n--\n\nLOCK TABLES `leitura` WRITE;\n/*!40000 ALTER TABLE `leitura` DISABLE KEYS */;\n/*!40000 ALTER TABLE `leitura` ENABLE KEYS */;\nUNLOCK TABLES;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\' */ ;\nDELIMITER ;;\n/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `{_db_name_}`.`leitura_AFTER_INSERT` AFTER INSERT ON `leitura` FOR EACH ROW\nBEGIN\n\nUPDATE {_db_name_}.sensor SET medida=NEW.valor WHERE id=NEW.sensor_id;\n\nEND */;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n\n--\n-- Table structure for table `niveis_controle`\n--\n\nDROP TABLE IF EXISTS `niveis_controle`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `niveis_controle` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `perfil` varchar(45) DEFAULT NULL,\n  `ativar` float(3,2) DEFAULT NULL,\n  `desativar` float(3,2) DEFAULT NULL,\n  PRIMARY KEY (`id`)\n) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `niveis_controle`\n--\n\nLOCK TABLES `niveis_controle` WRITE;\n/*!40000 ALTER TABLE `niveis_controle` DISABLE KEYS */;\nINSERT INTO `niveis_controle` VALUES (1,\'moderado\',0.50,0.80);\n/*!40000 ALTER TABLE `niveis_controle` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Table structure for table `reservatorio`\n--\n\nDROP TABLE IF EXISTS `reservatorio`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `reservatorio` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `identificacao` varchar(45) DEFAULT NULL,\n  `formato` enum(\'QUADRADO\',\'CILINDRICO\',\'CONICO\') DEFAULT NULL,\n  `altura` int(11) DEFAULT NULL,\n  `largura` int(11) DEFAULT NULL,\n  `comprimento` int(11) DEFAULT NULL,\n  `capacidade` int(11) DEFAULT NULL,\n  `razao_base` float(7,6) DEFAULT \'1.000000\',\n  `lamina` float(11,6) DEFAULT NULL,\n  PRIMARY KEY (`id`)\n) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `reservatorio`\n--\n\nLOCK TABLES `reservatorio` WRITE;\n/*!40000 ALTER TABLE `reservatorio` DISABLE KEYS */;\nINSERT INTO `reservatorio` VALUES (2,\'CAIXA_50L_PITCH\',\'QUADRADO\',33,29,60,42,1.000000,7),(1,\'CAIXA_500L\',\'QUADRADO\',75,100,100,500,1.000000,25),(3,\'CAIXA_10000L\',\'CILINDRICO\',328,200,200,10000,1.000000,10),(7,\'CAIXA \',\'QUADRADO\',275,320,350,26880,1.000000,40);\n/*!40000 ALTER TABLE `reservatorio` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Table structure for table `sensor`\n--\n\nDROP TABLE IF EXISTS `sensor`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `sensor` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `dispositivo_id` int(11) DEFAULT NULL,\n  `porta` enum(\'A\',\'B\',\'C\',\'D\',\'E\') DEFAULT \'D\',\n  `tipo` enum(\'SONICO\',\'FLUXO\',\'CONTATO\',\'TEMPERATURA\') DEFAULT NULL,\n  `apelido` varchar(45) DEFAULT NULL,\n  `reservatorio_id` int(11) DEFAULT NULL,\n  `medida` int(11) DEFAULT \'0\',\n  `disponivel` int(11) DEFAULT \'0\',\n  `meta_mensal` int(11) DEFAULT \'0\',\n  `limiar_temperatura` int(11) NOT NULL DEFAULT \'75\',\n  `atualizacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,\n  PRIMARY KEY (`id`),\n  KEY `dispositivo` (`dispositivo_id`,`apelido`,`tipo`)\n) ENGINE=MyISAM DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `sensor`\n--\n\nLOCK TABLES `sensor` WRITE;\n/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;\n/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;\nUNLOCK TABLES;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\' */ ;\nDELIMITER ;;\n/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `sensor_BEFORE_UPDATE` BEFORE UPDATE ON `sensor` FOR EACH ROW BEGIN\n\nSET @formato     = NULL;\nSET @altura      = NULL;\nSET @largura     = NULL;\nSET @comprimento = NULL;\nSET @razao_base  = NULL;\nSET @lamina      = NULL;\nSET @diferenca   = ABS(OLD.medida - NEW.medida);\nSET @fluxo       = IF(OLD.medida>NEW.medida,\'E\',\'S\');\nSET @litros      = 0;\n\nSELECT \n    formato, altura, largura, comprimento, razao_base, lamina\nINTO @formato , @altura , @largura , @comprimento , @razao_base , @lamina FROM\n    {_db_name_}.reservatorio\nWHERE\n    id = OLD.reservatorio_id;\n  \nIF (NEW.medida>@altura) THEN\n   SET NEW.medida = @altura;\nEND IF;\n\nSET @maximo = @altura - NEW.medida;\n  \nIF (OLD.tipo=\'SONICO\') THEN\n\nIF (@formato=\'QUADRADO\') THEN\n\n   SET @litros = ROUND(@largura*@comprimento*@diferenca/1000,3);\n   SET NEW.disponivel = ROUND(@largura*@comprimento*@maximo/1000,3);\n   IF (@diferenca!=0) THEN\n   INSERT INTO {_db_name_}.fluxo SET sensor_id=OLD.id,\n                                  fluxo=@fluxo,\n                                  valor=@litros;\n   END IF;\n\nEND IF;\n\nIF (@formato=\'CILINDRICO\') THEN\n\n   SET @raio = @largura/2;\n   SET @litros = ROUND((PI()*@diferenca*(@raio*@raio))/1000,3);\n   SET NEW.disponivel = ROUND((PI()*@maximo*(@raio*@raio))/1000,3);\n   IF (@diferenca!=0) THEN\n   INSERT INTO {_db_name_}.fluxo SET sensor_id=OLD.id,\n                                  fluxo=@fluxo,\n                                  valor=@litros;\n   END IF;\nEND IF;\n\nIF (@formato=\'CONICO\') THEN\n\n   SET @pos    = 1-(@altura-(NEW.medida-@lamina))/@altura;\n   SET @raio   = (@largura/2) - ((@largura/2)*(@razao_base*@pos));\n   SET @litros = ROUND((PI()*@diferenca*(@raio*@raio))/1000,3);\n   SET NEW.disponivel = ROUND((PI()*@maximo*(@raio*@raio))/1000,3);\n   IF (@diferenca!=0) THEN\n   INSERT INTO {_db_name_}.fluxo SET sensor_id=OLD.id,\n                                  fluxo=@fluxo,\n                                  valor=@litros;\n	END IF;\n\nEND IF;\n\nEND IF;\n\nIF (NEW.medida<>0) AND (OLD.tipo=\'FLUXO\') THEN\n\n   SET @litros = @medida;\n   INSERT INTO {_db_name_}.fluxo SET sensor_id=OLD.id,\n                                  fluxo=\'E\',\n                                  valor=@litros;\n\nEND IF;\n\nIF (OLD.tipo=\'CONTATO\') AND (@diferenca=1) THEN\n\n    SET @estado = IF(NEW.medida=1,\'LIGADA\',\'DESLIGADA\');\n    INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                    tipo_id=1,\n                                    valor=@estado;\n\nEND IF;\n\nIF (OLD.tipo=\'TEMPERATURA\') THEN\n\n    SET @tempo = 0;\n	SELECT {_db_name_}.sf_lastEvent(7, OLD.id) INTO @tempo;\n	IF (NEW.medida>=OLD.limiar_temperatura) AND ((@tempo>15) OR (@tempo IS NULL)) THEN\n      INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                        tipo_id=7,\n                                        valor=CONCAT(ROUND(NEW.medida,2),\' ºC\');\n   END IF;\n\nEND IF;\n\nIF (OLD.tipo=\'SONICO\') THEN\n\n   SET @flow  = NULL;\n   SET @tempo = NULL;\nSELECT {_db_name_}.sf_newFlow(OLD.id), {_db_name_}.sf_lastEvent(2, OLD.id) INTO @flow , @tempo;\n   \n   IF (@flow=0) AND ((@tempo>720) OR (@tempo IS NULL)) THEN\n      INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                        tipo_id=2;\n   END IF;\n    \n   SET @available = NULL;\n   SET @tempo     = NULL;\n   \nSELECT \n    {_db_name_}.sf_available(OLD.id, NEW.disponivel),\n    {_db_name_}.sf_lastEvent(5, OLD.id)\nINTO @available , @tempo;\n   \n   IF (@available<=50) AND ((@tempo>60) OR (@tempo IS NULL)) THEN\n      INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                        tipo_id=5,\n                                        valor=CONCAT(round(@available,2), \' %\');\n   END IF; \n   \n   IF (@fluxo=\'S\') THEN\n   \n   SET @tempo     = NULL;\n   \nSELECT \n    {_db_name_}.sf_available(OLD.id, @litros),\n    {_db_name_}.sf_lastEvent(4, OLD.id)\nINTO @available , @tempo;\n   \n   IF (@available>=10) AND ((@tempo>60) OR (@tempo IS NULL)) THEN\n      INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                        tipo_id=4,\n                                        valor=CONCAT(ROUND(@litros,0), \'L\');\n   END IF;\n   \n   IF (OLD.meta_mensal>0) THEN\n   \n   SET @mensal = NULL;\n   SET @tempo  = NULL;\n          \nSELECT {_db_name_}.sf_lastEvent(3, OLD.id),{_db_name_}.sf_waterUsed(OLD.id) INTO @tempo, @mensal;\n   \n   IF (@mensal>=OLD.meta_mensal) AND ((@tempo=0) OR (@tempo IS NULL)) THEN\n\n      INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                        tipo_id=3,\n                                        valor=CONCAT(@mensal, \'L\');\n   \n   END IF;\n   \n   END IF;\n   \n   IF (NEW.disponivel>@capacidade) THEN\n   \n      SET @tempo = NULL;\n      SELECT {_db_name_}.sf_lastEvent(6, OLD.id) INTO @tempo;\n      \n      IF (@tempo>60) OR (@tempo IS NULL) THEN\n      INSERT INTO {_db_name_}.evento SET sensor_id=OLD.id,\n                                        tipo_id=6,\n                                        valor=CONCAT(ROUND(NEW.disponivel,0), \'L\');\n   END IF;\n      \n   END IF;\n   \n   END IF;\n\nEND IF;\n\nEND */;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n\n--\n-- Table structure for table `tipo_evento`\n--\n\nDROP TABLE IF EXISTS `tipo_evento`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n/*!40101 SET character_set_client = utf8 */;\nCREATE TABLE `tipo_evento` (\n  `id` int(11) NOT NULL AUTO_INCREMENT,\n  `descricao` varchar(45) DEFAULT NULL,\n  `severidade` enum(\'A\',\'M\',\'U\') DEFAULT NULL,\n  `sigla` varchar(4) DEFAULT NULL,\n  PRIMARY KEY (`id`)\n) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;\n/*!40101 SET character_set_client = @saved_cs_client */;\n\n--\n-- Dumping data for table `tipo_evento`\n--\n\nLOCK TABLES `tipo_evento` WRITE;\n/*!40000 ALTER TABLE `tipo_evento` DISABLE KEYS */;\nINSERT INTO `tipo_evento` VALUES (1,\'MUDANÇA DE ESTADO DA BOMBA\',\'A\',\'BOMB\'),(2,\'FORNECIMENTO INTERROMPIDO\',\'M\',\'FINT\'),(3,\'META DE CONSUMO ATINGIDA\',\'A\',\'META\'),(4,\'PICO DE CONSUMO\',\'A\',\'PICO\'),(5,\'ALERTA DE NIVEL CRITICO\',\'U\',\'NCRI\'),(6,\'ALERTA DE TRANSBORDO\',\'U\',\'ATRA\'),(7,\'ALERTA TEMPERATURA ALTA\',\'U\',\'ATAL\');\n/*!40000 ALTER TABLE `tipo_evento` ENABLE KEYS */;\nUNLOCK TABLES;\n\n--\n-- Temporary view structure for view `view_saintTropez`\n--\n\nDROP TABLE IF EXISTS `view_saintTropez`;\n/*!50001 DROP VIEW IF EXISTS `view_saintTropez`*/;\nSET @saved_cs_client     = @@character_set_client;\nSET character_set_client = utf8;\n/*!50001 CREATE VIEW `view_saintTropez` AS SELECT \n 1 AS `Rotulo`,\n 1 AS `Lamina`*/;\nSET character_set_client = @saved_cs_client;\n\n--\n-- Dumping routines for database \'{_db_name_}\'\n--\n/*!50003 DROP FUNCTION IF EXISTS `sf_available` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_available`(_sensor_id INT, _atual INT) RETURNS float(6,2)\nBEGIN\n\nSET @maximo = NULL;\n\nSELECT capacidade INTO @maximo FROM {_db_name_}.reservatorio r LEFT JOIN {_db_name_}.sensor s ON (r.id=s.reservatorio_id)\nWHERE s.id=_sensor_id;\n\nRETURN ROUND(100*_atual/@maximo,2);\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_flow` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_flow`(_sensor_id INT) RETURNS tinyint(1)\nBEGIN\n\nSET @diferenca = NULL;\nSET @minutos   = NULL;\nSET @atual     = NULL;\nSET @lamina    = NULL;\n\n\n\nSELECT (v1-v2) ,ROUND(time_to_sec(timediff(d2,d1))/60,0),v2  INTO @diferenca,@minutos,@atual FROM\n(SELECT sensor_id,valor as v1,data as d1 FROM {_db_name_}.leitura WHERE sensor_id=_sensor_id ORDER BY id DESC Limit 1,1) A\nLEFT JOIN\n(SELECT sensor_id,valor as v2,data as d2 FROM {_db_name_}.leitura WHERE sensor_id=_sensor_id ORDER BY id DESC Limit 0,1) B\nON (A.sensor_id=B.sensor_id);\n\nSELECT lamina INTO @lamina FROM {_db_name_}.reservatorio r LEFT JOIN {_db_name_}.sensor s ON (r.id=s.reservatorio_id)\nWHERE s.id=_sensor_id;\n\nSET @flow = IF(@diferenca>0,1,IF(@minutos>=30,0,1));\nSET @flow = IF(@lamina>=@atual,1,@flow);\n\nRETURN @flow;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_jsonData` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8mb4 */ ;\n/*!50003 SET character_set_results = utf8mb4 */ ;\n/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_jsonData`(`_id` INT) RETURNS text CHARSET latin1\nBEGIN\n\nSET @json = \'{ \';\nSET @aspas = char(34);\nSET @disponivel     = NULL;\nSET @percentual     = NULL;\nSET @mensal         = NULL;\nSET @diario         = NULL;\nSET @horario        = NULL;\nSET @consumo_labels = NULL;\nSET @consumo_data   = NULL;\nSET @eventos        = NULL;\n\nSELECT disponivel INTO @disponivel FROM {_db_name_}.sensor WHERE id=_id;\n\nSELECT ROUND({_db_name_}.sf_available(_id,@disponivel),0) INTO @percentual;\n\nSELECT ROUND(SUM(valor),0) INTO @mensal  FROM {_db_name_}.fluxo WHERE sensor_id=_id and fluxo=\'S\' and data>=date_sub(now(), INTERVAL 30 DAY); \n\nSELECT ROUND(SUM(valor),0) INTO @diario  FROM {_db_name_}.fluxo WHERE sensor_id=_id and fluxo=\'S\' and date(data)=curdate(); \n\nSELECT ROUND(SUM(valor),0) INTO @horario FROM {_db_name_}.fluxo WHERE sensor_id=_id and fluxo=\'S\' and data>=date_sub(now(), INTERVAL 1 HOUR); \n\nSELECT GROUP_CONCAT(\'\'\',RIGHT(hora,2),\'\'\'), GROUP_CONCAT(valor) INTO @consumo_labels, @consumo_data \n\n       FROM  (SELECT DATE_FORMAT(data,\'%Y-%m-%d %H\') as hora,ROUND(SUM(valor)) as valor FROM {_db_name_}.fluxo\n\n             WHERE data>=date_sub(now(),INTERVAL 24 HOUR) and sensor_id=_id and fluxo=\'S\'\n\n             GROUP BY DATE_FORMAT(data,\'%Y-%m-%d %H\')) a;\n\n\n\nSET @json = CONCAT(@json,@aspas,\'disponivel\',@aspas,\' : \',IFNULL(@disponivel,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'percentual\',@aspas,\' : \',IFNULL(@percentual,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'mensal\'    ,@aspas,\' : \',IFNULL(@mensal,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'diario\'    ,@aspas,\' : \',IFNULL(@diario,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'horario\'   ,@aspas,\' : \',IFNULL(@horario,0),\' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'consumo\'   ,@aspas,\' : {\');\n\nSET @json = CONCAT(@json,@aspas,\'labels\'    ,@aspas,\' : [ \',IFNULL(@consumo_labels,\'\'),\' ],\');\n\nSET @json = CONCAT(@json,@aspas,\'data\'      ,@aspas,\' : [ \',IFNULL(@consumo_data,\'\'),\' ] },\');\n\nSET @json = CONCAT(@json,@aspas,\'eventos\'   ,@aspas,\' : [ {\',IFNULL(@eventos,\'\'), \' } ]\');\n\n\n\nSET @json = CONCAT(@json,\' }\');\n\n\n\nRETURN @json;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_lastEvent` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_lastEvent`(_event INT,_sensor INT) RETURNS int(11)\nBEGIN\n\n\n\nSET @tempo  = NULL;\n\nSET @mensal = NULL;\n\n\n\nSELECT \n\n    ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), data)) / 60,\n\n            0),\n\n    IF(YEAR(data) = YEAR(CURDATE())\n\n            AND MONTH(data) = MONTH(CURDATE()),\n\n        1,\n\n        0)\n\nINTO @tempo , @mensal FROM\n\n    {_db_name_}.evento\n\nWHERE\n\n    sensor_id = _sensor AND tipo_id = _event\n\nORDER BY id DESC\n\nLIMIT 0 , 1;\n\n\n\nIF (_event=3) THEN\n\n   SET @tempo = @mensal;\n\nEND IF;\n\n\n\nRETURN @tempo;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_newFlow` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8mb4 */ ;\n/*!50003 SET character_set_results = utf8mb4 */ ;\n/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_newFlow`(`_sensor_id` INT) RETURNS tinyint(4)\nBEGIN\n\n\n\nSET @lamina   = NULL;\n\nSET @newFluxo = NULL;\n\nSET @minimo   = NULL;\n\n\n\nSELECT SUM(IF(fluxo=\'E\',valor,(-1)*valor)) INTO @newFluxo FROM {_db_name_}.fluxo WHERE sensor_id=_sensor_id and data>=date_sub(now(), INTERVAL 1 HOUR)\n\nGROUP BY sensor_id;\n\n\n\nSELECT min(valor) into @minimo FROM {_db_name_}.leitura WHERE sensor_id=_sensor_id and data>=date_sub(now(), INTERVAL 1 HOUR)\n\nGROUP BY sensor_id;\n\n\n\nSELECT lamina INTO @lamina FROM {_db_name_}.reservatorio r LEFT JOIN {_db_name_}.sensor s ON (r.id=s.reservatorio_id)\n\nWHERE s.id=_sensor_id;\n\n\n\nSET @flow = IF(@newFluxo>=0,1,0);\n\nSET @flow = IF(@lamina>=@minimo,1,@flow);\n\n\n\nRETURN @flow;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_PercToDist` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_PercToDist`(_dispositivo INT, _perc FLOAT(3,2)) RETURNS int(11)\nBEGIN\n\n\n\nSET @valor       = 0;\n\nSET @formato     = NULL;\n\nSET @altura      = NULL;\n\nSET @largura     = NULL;\n\nSET @comprimento = NULL;\n\nSET @razao_base  = NULL;\n\nSET @lamina      = NULL;\n\nSET @capacidade  = NULL;\n\n\n\nSELECT \n\n    formato, altura, largura, comprimento, razao_base, lamina, capacidade\n\nINTO @formato , @altura , @largura , @comprimento , @razao_base , @lamina,@capacidade FROM\n\n    {_db_name_}.reservatorio r, {_db_name_}.sensor s\n\nWHERE\n\n   r.id=s.reservatorio_id AND\n\n   s.dispositivo_id=_dispositivo AND\n\n   s.tipo=\'SONICO\';\n\n    \n\nSET @litros = @capacidade * _perc;\n\n\n\nIF (@formato=\'QUADRADO\') THEN\n\n   SET @valor = @altura - ROUND((1000 * @litros)/(@largura*@comprimento),0);\n\nEND IF;\n\n\n\nIF (@formato=\'CILINDRICO\') THEN\n\n   SET @valor = @altura - ROUND((1000 * @litros)/((@largura/2)*(@largura/2)*pi()),0);\n\nEND IF;\n\n\n\nRETURN @valor;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_testCondition` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_testCondition`(_id INT) RETURNS tinyint(1)\nBEGIN\n\n\n\nSET @relativo = NULL;\n\nSET @codigo   = NULL;\n\nSET @condicao = NULL;\n\nSET @valor    = NULL;\n\nSET @ativa    = 0;\n\nSET @medida   = NULL;\n\n\n\nSELECT \n\n    relativo, codigo, condicao, valor\n\nINTO @relativo , @codigo , @condicao , @valor FROM\n\n    {_db_name_}.condicoes\n\nWHERE\n\n    id = _id;\n\n\n\nIF (@relativo=\'SENSOR\') THEN\n\n    \n\nSELECT \n\n    medida\n\nINTO @medida FROM\n\n    {_db_name_}.sensor\n\nWHERE\n\n    id = @codigo;\n\n\n\nEND IF;\n\n\n\nIF (@relativo=\'HORA\') THEN\n\n\n\n   SET @medida = TIME(NOW());\n\n\n\nEND IF;\n\n\n\nCASE @condicao\n\n    WHEN \'<\'  THEN SET @ativa = IF(@medida<@valor,1,0);\n\n    WHEN \'>\'  THEN SET @ativa = IF(@medida>@valor,1,0);\n\n    WHEN \'=\'  THEN SET @ativa = IF(@medida=@valor,1,0);\n\n    WHEN \'<>\' THEN SET @ativa = IF(@medida<>@valor,1,0);\n\n    WHEN \'<=\' THEN SET @ativa = IF(@medida<=@valor,1,0);\n\n    WHEN \'>=\' THEN SET @ativa = IF(@medida>=@valor,1,0);\n\n    ELSE\n\n       SET @ativa = 0;\n\n    END CASE;\n\n\n\nRETURN @ativa;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP FUNCTION IF EXISTS `sf_waterUsed` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` FUNCTION `sf_waterUsed`(_sensor_id INT) RETURNS int(11)\nBEGIN\n\n\n\nSET @mensal = NULL;\n\n\n\nSELECT \n\n    ROUND(SUM(valor),0)\n\nINTO @mensal FROM\n\n    {_db_name_}.fluxo\n\nWHERE\n\n    sensor_id = _sensor_id AND fluxo = \'S\'\n\n        AND MONTH(data) = MONTH(CURDATE())\n\n        AND YEAR(data) = YEAR(CURDATE());\n\n\n\nRETURN @mensal;\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP PROCEDURE IF EXISTS `sp_checkCondition` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8 */ ;\n/*!50003 SET character_set_results = utf8 */ ;\n/*!50003 SET collation_connection  = utf8_general_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` PROCEDURE `sp_checkCondition`(_sensor_id INT)\nBEGIN\n\n\n\nSELECT IF(condicoes=verdadeiras,1,0) as ativar FROM\n\n(SELECT SUM(1) as condicoes, SUM({_db_name_}.sf_testCondition(id)) as verdadeiras\n\nFROM {_db_name_}.condicoes WHERE sensor_id = _sensor_id) A;\n\n\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP PROCEDURE IF EXISTS `sp_jsonData` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8mb4 */ ;\n/*!50003 SET character_set_results = utf8mb4 */ ;\n/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jsonData`(IN `_id` INT)\n    NO SQL\nBEGIN\n\n\n\nSET @json = \'{ \';\n\nSET @aspas = char(34);\n\n\n\nSET @disponivel     = NULL;\n\nSET @percentual     = NULL;\n\nSET @mensal         = NULL;\n\nSET @diario         = NULL;\n\nSET @horario        = NULL;\n\nSET @consumo_labels = NULL;\n\nSET @consumo_data   = NULL;\n\nSET @eventos        = NULL;\n\n\n\nSET @json = CONCAT(@json,@aspas,\'disponivel\',@aspas,\' : \',IFNULL(@disponivel,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'percentual\',@aspas,\' : \',IFNULL(@percentual,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'mensal\'    ,@aspas,\' : \',IFNULL(@mensal,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'diario\'    ,@aspas,\' : \',IFNULL(@diario,0), \' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'horario\'   ,@aspas,\' : \',IFNULL(@horario,0),\' ,\');\n\nSET @json = CONCAT(@json,@aspas,\'consumo\'   ,@aspas,\' : {\');\n\nSET @json = CONCAT(@json,@aspas,\'labels\'    ,@aspas,\' : [ \',IFNULL(@consumo_labels,\'\'),\' ],\');\n\nSET @json = CONCAT(@json,@aspas,\'data\'      ,@aspas,\' : [ \',IFNULL(@consumo_data,\'\'),\' ] },\');\n\nSET @json = CONCAT(@json,@aspas,\'eventos\'   ,@aspas,\' : [ {\',IFNULL(@eventos,\'\'), \' } ]\');\n\n\n\nSET @json = CONCAT(@json,\' }\');\n\n\n\nSELECT @json as json;\n\n\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n/*!50003 DROP PROCEDURE IF EXISTS `whoIAm` */;\n/*!50003 SET @saved_cs_client      = @@character_set_client */ ;\n/*!50003 SET @saved_cs_results     = @@character_set_results */ ;\n/*!50003 SET @saved_col_connection = @@collation_connection */ ;\n/*!50003 SET character_set_client  = utf8mb4 */ ;\n/*!50003 SET character_set_results = utf8mb4 */ ;\n/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;\n/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;\n/*!50003 SET sql_mode              = \'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\' */ ;\nDELIMITER ;;\nCREATE DEFINER=`root`@`localhost` PROCEDURE `whoIAm`(IN `_mac` VARCHAR(20))\n    NO SQL\nBEGIN\n\n\n\n \n\n\n\nSET @id       = NULL;\n\n\n\nSET @controle = NULL;\n\n\n\nSET @funcao   = NULL;\n\n\n\n \n\n\n\nSELECT id,nivel_controle_id,IF(`slave`<>\'\',\'M\',\'S\') INTO @id,@controle,@funcao FROM {_db_name_}.dispositivo WHERE mac=_mac;\n\n\n\nIF (@id IS NULL) THEN\n\n\n\n    SELECT 0 as dispositivo_id;\n\n\n\nELSE\n\n\n\nSET @ativa    = 0;\n\n\n\nSET @desativa = 0;\n\n\n\n \n\n\n\nSELECT {_db_name_}.sf_PercToDist(d.id,n.ativar),\n\n\n\n       {_db_name_}.sf_PercToDist(d.id,n.desativar)\n\n\n\n  INTO @ativa, @desativa\n\n\n\n  FROM {_db_name_}.dispositivo d, niveis_controle n\n\n\n\nWHERE mac=_mac AND n.id=nivel_controle_id;\n\n\n\n \n\n\n\nSELECT mac,dispositivo_id as did,\n\n\n\nIF(`slave`<>\'\',`slave`,\'\') as \'slave\',\n\n\n\n#SUM(IF(porta=\'A\',id,0)) as iA, SUM(IF(porta=\'A\',tipo,0)) as tA,\n\n\n\n#SUM(IF(porta=\'B\',id,0)) as iB, SUM(IF(porta=\'B\',tipo,0)) as tB,\n\n\n\n#SUM(IF(porta=\'C\',id,0)) as iC, #SUM(IF(porta=\'C\',IF(tipo=4,limiar_temperatura,tipo),0)) as tC,\n\n\n\nSUM(IF(porta=\'D\',id,0)) as iD, SUM(IF(porta=\'D\',tipo,0)) as tD,\n\n\n\nSUM(IF(porta=\'E\',id,0)) as iE, SUM(IF(porta=\'E\',tipo,0)) as tE,\n\n\n\n@ativa as bot,\n\n\n\n@desativa as top\n\n\n\nFROM (\n\n\n\nSELECT mac,dispositivo_id,s.id,s.porta,IF(s.tipo=\'FLUXO\',1,IF(s.tipo=\'SONICO\',2,IF(s.tipo=\'CONTATO\',3,4))) as tipo,`slave`,s.limiar_temperatura\n\n\n\n  FROM {_db_name_}.dispositivo d LEFT JOIN {_db_name_}.sensor s ON (s.dispositivo_id=d.id)\n\n\n\nWHERE mac=_mac) A\n\n\n\nGROUP BY dispositivo_id, slave;\n\n\n\nEND IF;\n\n\n\n \n\n\n\nEND ;;\nDELIMITER ;\n/*!50003 SET sql_mode              = @saved_sql_mode */ ;\n/*!50003 SET character_set_client  = @saved_cs_client */ ;\n/*!50003 SET character_set_results = @saved_cs_results */ ;\n/*!50003 SET collation_connection  = @saved_col_connection */ ;\n\n--\n-- Final view structure for view `view_saintTropez`\n--\n\n/*!50001 DROP VIEW IF EXISTS `view_saintTropez`*/;\n/*!50001 SET @saved_cs_client          = @@character_set_client */;\n/*!50001 SET @saved_cs_results         = @@character_set_results */;\n/*!50001 SET @saved_col_connection     = @@collation_connection */;\n/*!50001 SET character_set_client      = utf8mb4 */;\n/*!50001 SET character_set_results     = utf8mb4 */;\n/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;\n/*!50001 CREATE ALGORITHM=UNDEFINED */\n/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */\n/*!50001 VIEW `view_saintTropez` AS select concat(`a`.`data`,\' - \',`a`.`hora`) AS `Rotulo`,(275 - `a`.`Lamina`) AS `Lamina` from (select cast(`{_db_name_}`.`leitura`.`data` as date) AS `data`,hour(`{_db_name_}`.`leitura`.`data`) AS `hora`,max(`{_db_name_}`.`leitura`.`valor`) AS `Lamina` from `{_db_name_}`.`leitura` where ((`{_db_name_}`.`leitura`.`sensor_id` = 1) and (`{_db_name_}`.`leitura`.`data` >= \'2018-07-27 13:00\')) group by cast(`{_db_name_}`.`leitura`.`data` as date),hour(`{_db_name_}`.`leitura`.`data`)) `a` */;\n/*!50001 SET character_set_client      = @saved_cs_client */;\n/*!50001 SET character_set_results     = @saved_cs_results */;\n/*!50001 SET collation_connection      = @saved_col_connection */;\n/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;\n\n/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;\n/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;\n/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;\n\n');
/*!40000 ALTER TABLE `hs_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hs_usuarios`
--

DROP TABLE IF EXISTS `hs_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hs_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `perfil` enum('USER','ADM') DEFAULT 'USER',
  `telegram` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_usuarios`
--

LOCK TABLES `hs_usuarios` WRITE;
/*!40000 ALTER TABLE `hs_usuarios` DISABLE KEYS */;
INSERT INTO `hs_usuarios` VALUES (1,'Adriano Emilio de Sousa','ewarmaster@gmail.com','e2818f83e17dc0d8cba0bc156a8fb3ea','ADM','48991475798'),(2,'Guilherme Souza','gsouza@haquasystems.net','97a42d3bf5ec8c6b7320a2a1089f6f6d','ADM',NULL),(59,'Adriano Emilio de Sousa','adriano.sousa@intelbras.com.br','202cb962ac59075b964b07152d234b70','ADM','');
/*!40000 ALTER TABLE `hs_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hs_usuarios_dispositivos`
--

DROP TABLE IF EXISTS `hs_usuarios_dispositivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hs_usuarios_dispositivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) DEFAULT NULL,
  `mac` varchar(17) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`),
  KEY `mac` (`mac`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_usuarios_dispositivos`
--

LOCK TABLES `hs_usuarios_dispositivos` WRITE;
/*!40000 ALTER TABLE `hs_usuarios_dispositivos` DISABLE KEYS */;
INSERT INTO `hs_usuarios_dispositivos` VALUES (1,1,'B4:E6:2D:2A:65:48'),(66,59,'B4:E6:2D:37:71:E6'),(3,1,'B4:E6:2D:37:7C:01'),(4,1,'DC:4F:22:55:A3:34'),(5,2,'B4:E6:2D:2A:65:48'),(11,2,'DC:4F:22:55:A3:34'),(10,2,'B4:E6:2D:37:7C:01'),(65,60,'B4:E6:2D:37:71:E6'),(62,57,'01');
/*!40000 ALTER TABLE `hs_usuarios_dispositivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservatorio`
--

DROP TABLE IF EXISTS `reservatorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservatorio` (
  `id` int(11) NOT NULL DEFAULT '0',
  `identificacao` varchar(45) DEFAULT NULL,
  `formato` enum('QUADRADO','CILINDRICO','CONICO') DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `largura` int(11) DEFAULT NULL,
  `comprimento` int(11) DEFAULT NULL,
  `capacidade` int(11) DEFAULT NULL,
  `razao_base` float(7,6) DEFAULT '1.000000',
  `lamina` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservatorio`
--

LOCK TABLES `reservatorio` WRITE;
/*!40000 ALTER TABLE `reservatorio` DISABLE KEYS */;
INSERT INTO `reservatorio` VALUES (2,'CAIXA_50L_PITCH','QUADRADO',33,29,60,42,1.000000,7),(1,'CAIXA_500L','QUADRADO',75,100,100,500,1.000000,25),(3,'CAIXA_10000L','CILINDRICO',328,200,200,10000,1.000000,10),(7,'CAIXA ','QUADRADO',275,320,350,26880,1.000000,40);
/*!40000 ALTER TABLE `reservatorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribes`
--

DROP TABLE IF EXISTS `subscribes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) DEFAULT NULL,
  `dev_name` varchar(50) DEFAULT NULL,
  `tb_name` varchar(50) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dispositivo` (`mac`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribes`
--

LOCK TABLES `subscribes` WRITE;
/*!40000 ALTER TABLE `subscribes` DISABLE KEYS */;
INSERT INTO `subscribes` VALUES (3,'DC:4F:22:55:A3:34','Res Dante Alighieri','res_dantealighieri','2019-01-16 18:28:00'),(60,'B4:E6:2D:37:71:E6','NOVO TESTE','DB_PID_000001','2019-02-22 00:34:17'),(1,'B4:E6:2D:37:7C:01','Casa Evaldo Luiz de Souza','casa_elsouza','2019-01-27 18:16:00'),(4,'B4:E6:2D:2A:65:48','Casa Guilherme Souza','casa_gsouza','2019-01-16 18:30:00'),(6,'84:F3:EB:10:57:8B','development','membrana','2019-01-16 18:28:00'),(8,'B4:E6:2D:37:16:6C','development_2','membrana','2019-01-27 22:11:00'),(59,'00','Teste nova conta','DB_PID_000003','2019-02-22 00:03:47'),(57,'01','ggg','DB_PID_000002','2019-02-21 23:30:12');
/*!40000 ALTER TABLE `subscribes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'subscriptions'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-26 20:28:06
