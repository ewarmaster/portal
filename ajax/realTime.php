<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];
$sensor = $_POST ['sensor'];
$hora = $_POST['time'];

$db = new db ();

$db->query ( "SELECT l.*,
                     f.fluxo,
                     f.valor as fvalor,
                     date_format(l.data,'%d/%m %H:%i') as dataFormatada 
                FROM $sensor.leitura l left join
                $sensor.fluxo f ON(l.sensor_id=f.sensor_id) and (l.data=f.data) ORDER BY l.data DESC LIMIT 10");
$data = $db->fetchAll();
?>
<div class="text-center">
<kbd><?=$hora;?></kbd>
<div class="display-3" id="lastUpdate"></div><br>
</div>
<div class="table-responsive">
<table class="table table-hover">
	<thead>
		<tr>
			<th>Data</th>
			<th>SID</th>
			<th>Leitura</th>
			<th>F</th>
			<th>Valor</th>
		</tr>
	</thead>
	<tbody><?php
    $v = "";
	foreach ( $data as $linha ) {
		if ($v=="") { 
			$v = $linha->valor;
		?>
			<script>
                $("#lastUpdate").html(<?=$v;?>);
			</script>
		<?php
		}
		?>
      <tr>
			<td><?=$linha->dataFormatada;?></td>
			<td><?=$linha->sensor_id;?></td>
			<td><?=$linha->valor;?></td>
			<td><?=$linha->fluxo;?></td>
			<td><?=$linha->fvalor;?></td>
		</tr>
      <?php
	}
	?>
    </tbody>
</table>
</div>