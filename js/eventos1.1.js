function updateEventList() {
	let sensor = $("#sensor").val();
	let evento = $("#evento").val();
	getDivContent("updateEventList", $("#eventList"), {
		sensor : sensor,
		evento : evento
	});
}

function updateRealTime() {
	let d = new Date();
	let sensor = $("#sensor").val();
	if (($("#switch").prop("checked")) && (sensor!="*")) {
	   getDivContent("realTime",$("#realTime"),{sensor : sensor, time : d.toLocaleTimeString()});
	   updateEventList();
	}
}

$(document).ready(function() {
	let timer = null;
	getDivContent("sensorList", $("#sensorList"));
	getDivContent("eventType", $("#typeEvent"));
	updateEventList();
	
	$("#switch").on("change",function() {
		if ($("#sensor").val()=="*") {
				$("#switch").prop("checked", false);
				alert("Para ativar o modo em tempo real é necessário escolher um sensor");
		}else{
			if ($("#switch").prop("checked"))
			   timer = setInterval(updateRealTime,2000);
			else
			   clearInterval(timer);
		}
	});
		
});
