

CREATE DATABASE  IF NOT EXISTS `DB_PID_000003` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `DB_PID_000003`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: DB_PID_000003
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `condicoes`
--

DROP TABLE IF EXISTS `condicoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condicoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) DEFAULT NULL,
  `relativo` enum('SENSOR','HORA') DEFAULT 'SENSOR',
  `codigo` int(11) DEFAULT NULL,
  `condicao` enum('=','<','>','<>','>=','<=') DEFAULT NULL,
  `valor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor` (`sensor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condicoes`
--

LOCK TABLES `condicoes` WRITE;
/*!40000 ALTER TABLE `condicoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `condicoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivo`
--

DROP TABLE IF EXISTS `dispositivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) DEFAULT NULL,
  `slave` varchar(17) DEFAULT NULL,
  `nivel_controle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dispositivo` (`mac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivo`
--

LOCK TABLES `dispositivo` WRITE;
/*!40000 ALTER TABLE `dispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispositivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) DEFAULT NULL,
  `tipo_id` varchar(45) DEFAULT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `valor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor` (`sensor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fluxo`
--

DROP TABLE IF EXISTS `fluxo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fluxo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sensor_id` int(11) DEFAULT NULL,
  `fluxo` enum('E','S') DEFAULT 'S',
  `valor` float(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor` (`sensor_id`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fluxo`
--

LOCK TABLES `fluxo` WRITE;
/*!40000 ALTER TABLE `fluxo` DISABLE KEYS */;
/*!40000 ALTER TABLE `fluxo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leitura`
--

DROP TABLE IF EXISTS `leitura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leitura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sensor_id` int(11) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leitura`
--

LOCK TABLES `leitura` WRITE;
/*!40000 ALTER TABLE `leitura` DISABLE KEYS */;
/*!40000 ALTER TABLE `leitura` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `DB_PID_000003`.`leitura_AFTER_INSERT` AFTER INSERT ON `leitura` FOR EACH ROW
BEGIN

UPDATE DB_PID_000003.sensor SET medida=NEW.valor WHERE id=NEW.sensor_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `niveis_controle`
--

DROP TABLE IF EXISTS `niveis_controle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `niveis_controle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(45) DEFAULT NULL,
  `ativar` float(3,2) DEFAULT NULL,
  `desativar` float(3,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `niveis_controle`
--

LOCK TABLES `niveis_controle` WRITE;
/*!40000 ALTER TABLE `niveis_controle` DISABLE KEYS */;
INSERT INTO `niveis_controle` VALUES (1,'moderado',0.50,0.80);
/*!40000 ALTER TABLE `niveis_controle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservatorio`
--

DROP TABLE IF EXISTS `reservatorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacao` varchar(45) DEFAULT NULL,
  `formato` enum('QUADRADO','CILINDRICO','CONICO') DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `largura` int(11) DEFAULT NULL,
  `comprimento` int(11) DEFAULT NULL,
  `capacidade` int(11) DEFAULT NULL,
  `razao_base` float(7,6) DEFAULT '1.000000',
  `lamina` float(11,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservatorio`
--

LOCK TABLES `reservatorio` WRITE;
/*!40000 ALTER TABLE `reservatorio` DISABLE KEYS */;
INSERT INTO `reservatorio` VALUES (2,'CAIXA_50L_PITCH','QUADRADO',33,29,60,42,1.000000,7),(1,'CAIXA_500L','QUADRADO',75,100,100,500,1.000000,25),(3,'CAIXA_10000L','CILINDRICO',328,200,200,10000,1.000000,10),(7,'CAIXA ','QUADRADO',275,320,350,26880,1.000000,40);
/*!40000 ALTER TABLE `reservatorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor`
--

DROP TABLE IF EXISTS `sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dispositivo_id` int(11) DEFAULT NULL,
  `porta` enum('A','B','C','D','E') DEFAULT 'D',
  `tipo` enum('SONICO','FLUXO','CONTATO','TEMPERATURA') DEFAULT NULL,
  `apelido` varchar(45) DEFAULT NULL,
  `reservatorio_id` int(11) DEFAULT NULL,
  `medida` int(11) DEFAULT '0',
  `disponivel` int(11) DEFAULT '0',
  `meta_mensal` int(11) DEFAULT '0',
  `limiar_temperatura` int(11) NOT NULL DEFAULT '75',
  `atualizacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dispositivo` (`dispositivo_id`,`apelido`,`tipo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor`
--

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`dev1`@`localhost`*/ /*!50003 TRIGGER `sensor_BEFORE_UPDATE` BEFORE UPDATE ON `sensor` FOR EACH ROW BEGIN

SET @formato     = NULL;
SET @altura      = NULL;
SET @largura     = NULL;
SET @comprimento = NULL;
SET @razao_base  = NULL;
SET @lamina      = NULL;
SET @diferenca   = ABS(OLD.medida - NEW.medida);
SET @fluxo       = IF(OLD.medida>NEW.medida,'E','S');
SET @litros      = 0;

SELECT 
    formato, altura, largura, comprimento, razao_base, lamina
INTO @formato , @altura , @largura , @comprimento , @razao_base , @lamina FROM
    DB_PID_000003.reservatorio
WHERE
    id = OLD.reservatorio_id;
  
IF (NEW.medida>@altura) THEN
   SET NEW.medida = @altura;
END IF;

SET @maximo = @altura - NEW.medida;
  
IF (OLD.tipo='SONICO') THEN

IF (@formato='QUADRADO') THEN

   SET @litros = ROUND(@largura*@comprimento*@diferenca/1000,3);
   SET NEW.disponivel = ROUND(@largura*@comprimento*@maximo/1000,3);
   IF (@diferenca!=0) THEN
   INSERT INTO DB_PID_000003.fluxo SET sensor_id=OLD.id,
                                  fluxo=@fluxo,
                                  valor=@litros;
   END IF;

END IF;

IF (@formato='CILINDRICO') THEN

   SET @raio = @largura/2;
   SET @litros = ROUND((PI()*@diferenca*(@raio*@raio))/1000,3);
   SET NEW.disponivel = ROUND((PI()*@maximo*(@raio*@raio))/1000,3);
   IF (@diferenca!=0) THEN
   INSERT INTO DB_PID_000003.fluxo SET sensor_id=OLD.id,
                                  fluxo=@fluxo,
                                  valor=@litros;
   END IF;
END IF;

IF (@formato='CONICO') THEN

   SET @pos    = 1-(@altura-(NEW.medida-@lamina))/@altura;
   SET @raio   = (@largura/2) - ((@largura/2)*(@razao_base*@pos));
   SET @litros = ROUND((PI()*@diferenca*(@raio*@raio))/1000,3);
   SET NEW.disponivel = ROUND((PI()*@maximo*(@raio*@raio))/1000,3);
   IF (@diferenca!=0) THEN
   INSERT INTO DB_PID_000003.fluxo SET sensor_id=OLD.id,
                                  fluxo=@fluxo,
                                  valor=@litros;
	END IF;

END IF;

END IF;

IF (NEW.medida<>0) AND (OLD.tipo='FLUXO') THEN

   SET @litros = @medida;
   INSERT INTO DB_PID_000003.fluxo SET sensor_id=OLD.id,
                                  fluxo='E',
                                  valor=@litros;

END IF;

IF (OLD.tipo='CONTATO') AND (@diferenca=1) THEN

    SET @estado = IF(NEW.medida=1,'LIGADA','DESLIGADA');
    INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                    tipo_id=1,
                                    valor=@estado;

END IF;

IF (OLD.tipo='TEMPERATURA') THEN

    SET @tempo = 0;
	SELECT DB_PID_000003.sf_lastEvent(7, OLD.id) INTO @tempo;
	IF (NEW.medida>=OLD.limiar_temperatura) AND ((@tempo>15) OR (@tempo IS NULL)) THEN
      INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                        tipo_id=7,
                                        valor=CONCAT(ROUND(NEW.medida,2),' ºC');
   END IF;

END IF;

IF (OLD.tipo='SONICO') THEN

   SET @flow  = NULL;
   SET @tempo = NULL;
SELECT DB_PID_000003.sf_newFlow(OLD.id), DB_PID_000003.sf_lastEvent(2, OLD.id) INTO @flow , @tempo;
   
   IF (@flow=0) AND ((@tempo>720) OR (@tempo IS NULL)) THEN
      INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                        tipo_id=2;
   END IF;
    
   SET @available = NULL;
   SET @tempo     = NULL;
   
SELECT 
    DB_PID_000003.sf_available(OLD.id, NEW.disponivel),
    DB_PID_000003.sf_lastEvent(5, OLD.id)
INTO @available , @tempo;
   
   IF (@available<=50) AND ((@tempo>60) OR (@tempo IS NULL)) THEN
      INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                        tipo_id=5,
                                        valor=CONCAT(round(@available,2), ' %');
   END IF; 
   
   IF (@fluxo='S') THEN
   
   SET @tempo     = NULL;
   
SELECT 
    DB_PID_000003.sf_available(OLD.id, @litros),
    DB_PID_000003.sf_lastEvent(4, OLD.id)
INTO @available , @tempo;
   
   IF (@available>=10) AND ((@tempo>60) OR (@tempo IS NULL)) THEN
      INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                        tipo_id=4,
                                        valor=CONCAT(ROUND(@litros,0), 'L');
   END IF;
   
   IF (OLD.meta_mensal>0) THEN
   
   SET @mensal = NULL;
   SET @tempo  = NULL;
          
SELECT DB_PID_000003.sf_lastEvent(3, OLD.id),DB_PID_000003.sf_waterUsed(OLD.id) INTO @tempo, @mensal;
   
   IF (@mensal>=OLD.meta_mensal) AND ((@tempo=0) OR (@tempo IS NULL)) THEN

      INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                        tipo_id=3,
                                        valor=CONCAT(@mensal, 'L');
   
   END IF;
   
   END IF;
   
   IF (NEW.disponivel>@capacidade) THEN
   
      SET @tempo = NULL;
      SELECT DB_PID_000003.sf_lastEvent(6, OLD.id) INTO @tempo;
      
      IF (@tempo>60) OR (@tempo IS NULL) THEN
      INSERT INTO DB_PID_000003.evento SET sensor_id=OLD.id,
                                        tipo_id=6,
                                        valor=CONCAT(ROUND(NEW.disponivel,0), 'L');
   END IF;
      
   END IF;
   
   END IF;

END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tipo_evento`
--

DROP TABLE IF EXISTS `tipo_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL,
  `severidade` enum('A','M','U') DEFAULT NULL,
  `sigla` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_evento`
--

LOCK TABLES `tipo_evento` WRITE;
/*!40000 ALTER TABLE `tipo_evento` DISABLE KEYS */;
INSERT INTO `tipo_evento` VALUES (1,'MUDANÇA DE ESTADO DA BOMBA','A','BOMB'),(2,'FORNECIMENTO INTERROMPIDO','M','FINT'),(3,'META DE CONSUMO ATINGIDA','A','META'),(4,'PICO DE CONSUMO','A','PICO'),(5,'ALERTA DE NIVEL CRITICO','U','NCRI'),(6,'ALERTA DE TRANSBORDO','U','ATRA'),(7,'ALERTA TEMPERATURA ALTA','U','ATAL');
/*!40000 ALTER TABLE `tipo_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_saintTropez`
--

DROP TABLE IF EXISTS `view_saintTropez`;
/*!50001 DROP VIEW IF EXISTS `view_saintTropez`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_saintTropez` AS SELECT 
 1 AS `Rotulo`,
 1 AS `Lamina`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'DB_PID_000003'
--
/*!50003 DROP FUNCTION IF EXISTS `sf_available` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_available`(_sensor_id INT, _atual INT) RETURNS float(6,2)
BEGIN



SET @maximo = NULL;



SELECT capacidade INTO @maximo FROM DB_PID_000003.reservatorio r LEFT JOIN DB_PID_000003.sensor s ON (r.id=s.reservatorio_id)

WHERE s.id=_sensor_id;



RETURN ROUND(100*_atual/@maximo,2);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_flow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_flow`(_sensor_id INT) RETURNS tinyint(1)
BEGIN



SET @diferenca = NULL;

SET @minutos   = NULL;

SET @atual     = NULL;

SET @lamina    = NULL;



SELECT (v1-v2) ,ROUND(time_to_sec(timediff(d2,d1))/60,0),v2  INTO @diferenca,@minutos,@atual FROM

(SELECT sensor_id,valor as v1,data as d1 FROM DB_PID_000003.leitura WHERE sensor_id=_sensor_id ORDER BY id DESC Limit 1,1) A

LEFT JOIN

(SELECT sensor_id,valor as v2,data as d2 FROM DB_PID_000003.leitura WHERE sensor_id=_sensor_id ORDER BY id DESC Limit 0,1) B

ON (A.sensor_id=B.sensor_id);



SELECT lamina INTO @lamina FROM DB_PID_000003.reservatorio r LEFT JOIN DB_PID_000003.sensor s ON (r.id=s.reservatorio_id)

WHERE s.id=_sensor_id;



SET @flow = IF(@diferenca>0,1,IF(@minutos>=30,0,1));

SET @flow = IF(@lamina>=@atual,1,@flow);



RETURN @flow;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_jsonData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_jsonData`(`_id` INT) RETURNS text CHARSET latin1
BEGIN



SET @json = '{ ';

SET @aspas = char(34);



SET @disponivel     = NULL;

SET @percentual     = NULL;

SET @mensal         = NULL;

SET @diario         = NULL;

SET @horario        = NULL;

SET @consumo_labels = NULL;

SET @consumo_data   = NULL;

SET @eventos        = NULL;



SELECT disponivel INTO @disponivel FROM DB_PID_000003.sensor WHERE id=_id;

SELECT ROUND(DB_PID_000003.sf_available(_id,@disponivel),0) INTO @percentual;

SELECT ROUND(SUM(valor),0) INTO @mensal  FROM DB_PID_000003.fluxo WHERE sensor_id=_id and fluxo='S' and data>=date_sub(now(), INTERVAL 30 DAY); 

SELECT ROUND(SUM(valor),0) INTO @diario  FROM DB_PID_000003.fluxo WHERE sensor_id=_id and fluxo='S' and date(data)=curdate(); 

SELECT ROUND(SUM(valor),0) INTO @horario FROM DB_PID_000003.fluxo WHERE sensor_id=_id and fluxo='S' and data>=date_sub(now(), INTERVAL 1 HOUR); 

SELECT GROUP_CONCAT(''',RIGHT(hora,2),'''), GROUP_CONCAT(valor) INTO @consumo_labels, @consumo_data 

       FROM  (SELECT DATE_FORMAT(data,'%Y-%m-%d %H') as hora,ROUND(SUM(valor)) as valor FROM DB_PID_000003.fluxo

             WHERE data>=date_sub(now(),INTERVAL 24 HOUR) and sensor_id=_id and fluxo='S'

             GROUP BY DATE_FORMAT(data,'%Y-%m-%d %H')) a;



SET @json = CONCAT(@json,@aspas,'disponivel',@aspas,' : ',IFNULL(@disponivel,0), ' ,');

SET @json = CONCAT(@json,@aspas,'percentual',@aspas,' : ',IFNULL(@percentual,0), ' ,');

SET @json = CONCAT(@json,@aspas,'mensal'    ,@aspas,' : ',IFNULL(@mensal,0), ' ,');

SET @json = CONCAT(@json,@aspas,'diario'    ,@aspas,' : ',IFNULL(@diario,0), ' ,');

SET @json = CONCAT(@json,@aspas,'horario'   ,@aspas,' : ',IFNULL(@horario,0),' ,');

SET @json = CONCAT(@json,@aspas,'consumo'   ,@aspas,' : {');

SET @json = CONCAT(@json,@aspas,'labels'    ,@aspas,' : [ ',IFNULL(@consumo_labels,''),' ],');

SET @json = CONCAT(@json,@aspas,'data'      ,@aspas,' : [ ',IFNULL(@consumo_data,''),' ] },');

SET @json = CONCAT(@json,@aspas,'eventos'   ,@aspas,' : [ {',IFNULL(@eventos,''), ' } ]');



SET @json = CONCAT(@json,' }');



RETURN @json;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_lastEvent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_lastEvent`(_event INT,_sensor INT) RETURNS int(11)
BEGIN



SET @tempo  = NULL;

SET @mensal = NULL;



SELECT 

    ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), data)) / 60,

            0),

    IF(YEAR(data) = YEAR(CURDATE())

            AND MONTH(data) = MONTH(CURDATE()),

        1,

        0)

INTO @tempo , @mensal FROM

    DB_PID_000003.evento

WHERE

    sensor_id = _sensor AND tipo_id = _event

ORDER BY id DESC

LIMIT 0 , 1;



IF (_event=3) THEN

   SET @tempo = @mensal;

END IF;



RETURN @tempo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_newFlow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_newFlow`(`_sensor_id` INT) RETURNS tinyint(4)
BEGIN



SET @lamina   = NULL;

SET @newFluxo = NULL;

SET @minimo   = NULL;



SELECT SUM(IF(fluxo='E',valor,(-1)*valor)) INTO @newFluxo FROM DB_PID_000003.fluxo WHERE sensor_id=_sensor_id and data>=date_sub(now(), INTERVAL 1 HOUR)

GROUP BY sensor_id;



SELECT min(valor) into @minimo FROM DB_PID_000003.leitura WHERE sensor_id=_sensor_id and data>=date_sub(now(), INTERVAL 1 HOUR)

GROUP BY sensor_id;



SELECT lamina INTO @lamina FROM DB_PID_000003.reservatorio r LEFT JOIN DB_PID_000003.sensor s ON (r.id=s.reservatorio_id)

WHERE s.id=_sensor_id;



SET @flow = IF(@newFluxo>=0,1,0);

SET @flow = IF(@lamina>=@minimo,1,@flow);



RETURN @flow;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_PercToDist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_PercToDist`(_dispositivo INT, _perc FLOAT(3,2)) RETURNS int(11)
BEGIN



SET @valor       = 0;

SET @formato     = NULL;

SET @altura      = NULL;

SET @largura     = NULL;

SET @comprimento = NULL;

SET @razao_base  = NULL;

SET @lamina      = NULL;

SET @capacidade  = NULL;



SELECT 

    formato, altura, largura, comprimento, razao_base, lamina, capacidade

INTO @formato , @altura , @largura , @comprimento , @razao_base , @lamina,@capacidade FROM

    DB_PID_000003.reservatorio r, DB_PID_000003.sensor s

WHERE

   r.id=s.reservatorio_id AND

   s.dispositivo_id=_dispositivo AND

   s.tipo='SONICO';

    

SET @litros = @capacidade * _perc;



IF (@formato='QUADRADO') THEN

   SET @valor = @altura - ROUND((1000 * @litros)/(@largura*@comprimento),0);

END IF;



IF (@formato='CILINDRICO') THEN

   SET @valor = @altura - ROUND((1000 * @litros)/((@largura/2)*(@largura/2)*pi()),0);

END IF;



RETURN @valor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_testCondition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_testCondition`(_id INT) RETURNS tinyint(1)
BEGIN



SET @relativo = NULL;

SET @codigo   = NULL;

SET @condicao = NULL;

SET @valor    = NULL;

SET @ativa    = 0;

SET @medida   = NULL;



SELECT 

    relativo, codigo, condicao, valor

INTO @relativo , @codigo , @condicao , @valor FROM

    DB_PID_000003.condicoes

WHERE

    id = _id;



IF (@relativo='SENSOR') THEN

    

SELECT 

    medida

INTO @medida FROM

    DB_PID_000003.sensor

WHERE

    id = @codigo;



END IF;



IF (@relativo='HORA') THEN



   SET @medida = TIME(NOW());



END IF;



CASE @condicao

    WHEN '<'  THEN SET @ativa = IF(@medida<@valor,1,0);

    WHEN '>'  THEN SET @ativa = IF(@medida>@valor,1,0);

    WHEN '='  THEN SET @ativa = IF(@medida=@valor,1,0);

    WHEN '<>' THEN SET @ativa = IF(@medida<>@valor,1,0);

    WHEN '<=' THEN SET @ativa = IF(@medida<=@valor,1,0);

    WHEN '>=' THEN SET @ativa = IF(@medida>=@valor,1,0);

    ELSE

       SET @ativa = 0;

    END CASE;



RETURN @ativa;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_waterUsed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_waterUsed`(_sensor_id INT) RETURNS int(11)
BEGIN



SET @mensal = NULL;



SELECT 

    ROUND(SUM(valor),0)

INTO @mensal FROM

    DB_PID_000003.fluxo

WHERE

    sensor_id = _sensor_id AND fluxo = 'S'

        AND MONTH(data) = MONTH(CURDATE())

        AND YEAR(data) = YEAR(CURDATE());



RETURN @mensal;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_checkCondition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_checkCondition`(_sensor_id INT)
BEGIN



SELECT IF(condicoes=verdadeiras,1,0) as ativar FROM

(SELECT SUM(1) as condicoes, SUM(DB_PID_000003.sf_testCondition(id)) as verdadeiras

FROM DB_PID_000003.condicoes WHERE sensor_id = _sensor_id) A;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_jsonData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jsonData`(IN `_id` INT)
    NO SQL
BEGIN



SET @json = '{ ';

SET @aspas = char(34);



SET @disponivel     = NULL;

SET @percentual     = NULL;

SET @mensal         = NULL;

SET @diario         = NULL;

SET @horario        = NULL;

SET @consumo_labels = NULL;

SET @consumo_data   = NULL;

SET @eventos        = NULL;



SET @json = CONCAT(@json,@aspas,'disponivel',@aspas,' : ',IFNULL(@disponivel,0), ' ,');

SET @json = CONCAT(@json,@aspas,'percentual',@aspas,' : ',IFNULL(@percentual,0), ' ,');

SET @json = CONCAT(@json,@aspas,'mensal'    ,@aspas,' : ',IFNULL(@mensal,0), ' ,');

SET @json = CONCAT(@json,@aspas,'diario'    ,@aspas,' : ',IFNULL(@diario,0), ' ,');

SET @json = CONCAT(@json,@aspas,'horario'   ,@aspas,' : ',IFNULL(@horario,0),' ,');

SET @json = CONCAT(@json,@aspas,'consumo'   ,@aspas,' : {');

SET @json = CONCAT(@json,@aspas,'labels'    ,@aspas,' : [ ',IFNULL(@consumo_labels,''),' ],');

SET @json = CONCAT(@json,@aspas,'data'      ,@aspas,' : [ ',IFNULL(@consumo_data,''),' ] },');

SET @json = CONCAT(@json,@aspas,'eventos'   ,@aspas,' : [ {',IFNULL(@eventos,''), ' } ]');



SET @json = CONCAT(@json,' }');



SELECT @json as json;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `whoIAm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `whoIAm`(IN `_mac` VARCHAR(20))
    NO SQL
BEGIN



 



SET @id       = NULL;



SET @controle = NULL;



SET @funcao   = NULL;



 



SELECT id,nivel_controle_id,IF(`slave`<>'','M','S') INTO @id,@controle,@funcao FROM DB_PID_000003.dispositivo WHERE mac=_mac;



IF (@id IS NULL) THEN



    SELECT 0 as dispositivo_id;



ELSE



SET @ativa    = 0;



SET @desativa = 0;



 



SELECT DB_PID_000003.sf_PercToDist(d.id,n.ativar),



       DB_PID_000003.sf_PercToDist(d.id,n.desativar)



  INTO @ativa, @desativa



  FROM DB_PID_000003.dispositivo d, niveis_controle n



WHERE mac=_mac AND n.id=nivel_controle_id;



 



SELECT mac,dispositivo_id as did,



IF(`slave`<>'',`slave`,'') as 'slave',



#SUM(IF(porta='A',id,0)) as iA, SUM(IF(porta='A',tipo,0)) as tA,



#SUM(IF(porta='B',id,0)) as iB, SUM(IF(porta='B',tipo,0)) as tB,



#SUM(IF(porta='C',id,0)) as iC, #SUM(IF(porta='C',IF(tipo=4,limiar_temperatura,tipo),0)) as tC,



SUM(IF(porta='D',id,0)) as iD, SUM(IF(porta='D',tipo,0)) as tD,



SUM(IF(porta='E',id,0)) as iE, SUM(IF(porta='E',tipo,0)) as tE,



@ativa as bot,



@desativa as top



FROM (



SELECT mac,dispositivo_id,s.id,s.porta,IF(s.tipo='FLUXO',1,IF(s.tipo='SONICO',2,IF(s.tipo='CONTATO',3,4))) as tipo,`slave`,s.limiar_temperatura



  FROM DB_PID_000003.dispositivo d LEFT JOIN DB_PID_000003.sensor s ON (s.dispositivo_id=d.id)



WHERE mac=_mac) A



GROUP BY dispositivo_id, slave;



END IF;



 



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_saintTropez`
--

/*!50001 DROP VIEW IF EXISTS `view_saintTropez`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_saintTropez` AS select concat(`a`.`data`,' - ',`a`.`hora`) AS `Rotulo`,(275 - `a`.`Lamina`) AS `Lamina` from (select cast(`DB_PID_000003`.`leitura`.`data` as date) AS `data`,hour(`DB_PID_000003`.`leitura`.`data`) AS `hora`,max(`DB_PID_000003`.`leitura`.`valor`) AS `Lamina` from `DB_PID_000003`.`leitura` where ((`DB_PID_000003`.`leitura`.`sensor_id` = 1) and (`DB_PID_000003`.`leitura`.`data` >= '2018-07-27 13:00')) group by cast(`DB_PID_000003`.`leitura`.`data` as date),hour(`DB_PID_000003`.`leitura`.`data`)) `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

