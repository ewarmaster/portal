<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];

$db = new db ();
$db->query ( "SELECT s.dev_name,s.tb_name FROM subscriptions.hs_usuarios_dispositivos d
		LEFT JOIN subscriptions.subscribes s ON (d.mac=s.mac)
		WHERE d.usuario=$user_id" );

$data = $db->fetchAll ();
?>
<label>Sensor</label>
<select id="sensor" class="custom-select">
	<option value="*">DE TODOS OS DISPOSITIVOS</option>
<?php
foreach ( $data as $linha ) {
	?>
	<option value="<?=$linha->tb_name;?>">
	<?=$linha->dev_name;?></option>
<?php
}
?>
</select>
<script>
$("#sensor").change(function() {
	updateEventList();
});
</script>