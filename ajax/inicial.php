<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];
$db = new db ();
$db->query ( "SELECT d.*,s.dev_name FROM subscriptions.hs_usuarios_dispositivos d
                          LEFT JOIN subscriptions.subscribes s ON (d.mac=s.mac) 
             WHERE d.usuario=$user_id" );
$data = $db->fetchAll ();
?>
<div class="row">
	<div class="col-sm-12">
		<h1>Seus Dispositivos</h1>
		<div class="card-deck">
<?php
foreach ( $data as $disp ) {
	?>
<div class="shadow card col-sm-3 p-2">
				<img class="card-img-top" src="img/dispositivo.png">
				<div class="card-body">
					<h5 class="card-title"><?=$disp->dev_name;?></h5>
					<p class="card-text"><?=$disp->mac;?></p>
					<button class="btn btn-primary" id="btnStatus"
						value="<?=$disp->mac;?>">Ver Status</button>
				</div>
			</div>
<?php } ?>
</div>
	</div>
</div>
<script>
$(document).ready(function () {
	
	$("button").click(function() {
		if (this.id=="btnStatus") 
			getDivContent('status',$("#main"),{mac : $(this).attr('value')});
	});
});
</script>
