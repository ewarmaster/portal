<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];
$user_priv = $_SESSION ['user_priv'];
$db = new db ();
$db->query ( "SELECT d.*,s.dev_name FROM subscriptions.hs_usuarios_dispositivos d
                          LEFT JOIN subscriptions.subscribes s ON (d.mac=s.mac) 
             WHERE d.usuario=$user_id" );
$data = $db->fetchAll ();

?>
<div class="row">
	<div class="col-lg-12">
		<h1>Eventos</h1>
		<div class="row">
			<form id="eventForm" class="shadow p-3 offset-lg-1 col-lg-5 mr-3">
				<div class="form-group" id="sensorList">
					<label>Sensor</label> <select id="sensor" class="custom-select">
						<option value="*" selected>DE TODOS OS DISPOSITIVOS</option>
					</select>
				</div>
				<div class="form-group" id="typeEvent">
					<label>Evento</label> <select id="evento" class="custom-select">
						<option value="*" selected>QUALQUER EVENTO</option>
					</select>
				</div>
				<hr class="bg-primary mt-3">
				<div id="eventList"><</div>
			</form>
			<?php if ($user_priv=="ADM") {?>
			<div class="container shadow p-3 col-lg-5">
				<form action="/action_page.php">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="switch"
							name="example" value="ativo"> <label class="custom-control-label"
							for="switch">Modo em Tempo Real</label>
					</div>
				</form>
				<hr class="bg-primary mt-3">
				<div id="realTime"></div>
			</div>
			<?php
			}
			?>		
		</div>
	</div>
</div>

<script src="js/eventos1.1.js"></script>