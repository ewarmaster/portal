$(document).ready(function() {
	$("#contaForm").submit(function(e) {
		e.preventDefault();
		let nome         = $("#nome").val();
		let email        = $("#email").val();
		let telegram     = $("#telegram").val();
		let pwd          = $("#pwdcheck").val();
		let newpwd       = $("#newpwd").val();
		let newpwd_again = $("#newpwd_again").val();
		$.ajax({
			type:"POST", 
			data: { nome        : nome, 
				    email       : email, 
				    telegram    : telegram,
				    senha       : pwd,
				    novasenha   : newpwd,
				    repetesenha : newpwd_again},
			url:"ajax/updateAccount.php", 
			dataType:'html', 
			success:function(html){
				getDivContent("conta",$("#main"), {retorno : html});
			}
		});
		
	})
});