<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];
$sensor = $_POST ['sensor'];
$evento = $_POST ['evento'];

$db = new db ();

$db->query ( "SELECT s.tb_name FROM subscriptions.hs_usuarios_dispositivos d
		LEFT JOIN subscriptions.subscribes s ON (d.mac=s.mac)
		WHERE d.usuario=$user_id" );

$campos = "*,date_format(data,'%d/%m %H:%i') as dataFormatada";

if (($sensor == "*") && ($db->numRows () > 1)) {
	$devs = $db->numRows ();
	$sql = "SELECT * FROM (";
	$data = $db->fetchAll ();
	$counter = 1;
	$anybase = "";
	foreach ( $data as $base ) {
		$sql .= "SELECT $campos FROM $base->tb_name.evento";
		$anybase = ($anybase=="") ? $base->tb_name : $anybase;
		$counter ++;
		$sql .= ($counter <= $devs) ? " UNION " : "";
	}
	$sql .= ") A LEFT JOIN $anybase.tipo_evento B ON (A.tipo_id=B.id)";
} elseif ($sensor == "*") {
	$data= $db->fetch();
	$sql = " SELECT $campos FROM $data->tb_name.evento A LEFT JOIN $data->tb_name.tipo_evento B ON (A.tipo_id=B.id)";
} else
	$sql = " SELECT $campos FROM $sensor.evento A LEFT JOIN $sensor.tipo_evento B ON (A.tipo_id=B.id)";
$sql .= " WHERE tipo_id='$evento' OR '$evento'='*' ORDER BY data DESC LIMIT 10";
$db->query ( $sql );
$data = $db->fetchAll ();
?>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Data</th>
			<th>Evento</th>
			<th>Valor</th>
		</tr>
	</thead>
	<tbody><?php
	foreach ( $data as $linha ) {
		?>
      <tr>
			<td><?=$linha->dataFormatada;?></td>
			<td><span class="evento_<?=$linha->severidade;?> glyphicon glyphicon-stop pr-2"></span><?=$linha->descricao;?></td>
			<td><?=$linha->valor;?></td>
		</tr>
      <?php
	}
	?>
    </tbody>
</table>