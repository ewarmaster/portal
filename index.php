<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<title>Portal Hágua Net - 1.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/index.css">
<link rel="icon" type="image/png" sizes="96x96" href="favicon.png">
<script src="js/index1.0.js"></script>

</head>
<?php
  $res = isset($_GET['result']) ? $_GET['result'] : 4;
  $msg = [ 0=>"Usuário não informado/não encontrado",
           1=>"Senha Inválida",
           2=>"Sucesso",
           3=>"Logout realizado",
           5=>"Registro efetuado com sucesso",
           6=>"Não foi possível registrar nova conta",
           7=>"Dispositivo não registrado na rede Haqua",
           8=>"Dispositivo já associado a um cliente"];
?>
<body>

	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true"
		style="display: none;">
		<div class="modal-dialog">
			<div class="loginmodal-container">
				<h1>Logar na sua Conta</h1>
				<br>
				<form action="system.php" method="post">
					<input type="text" name="user" placeholder="Usuário" id="user">
					<input type="password" name="pass" placeholder="Senha"> <input
						type="submit" name="login" class="login loginmodal-submit"
						value="Logar">
				</form>

				<div class="login-help">
					<a href="register.php">Registrar</a> - <a
						href="#">Esqueceu Senha</a>
				</div>
				<?php if ($res!=4) {?>
				<div class="alert alert-warning" style="margin-top: 10px;">
					<?=$msg[$res];?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>

</body>
</html>
