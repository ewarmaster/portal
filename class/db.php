<?php
define('SERVER_MYSQL', 'localhost');
define('MYSQL_USER', 'dev1');
define('MYSQL_PASS', 'Haqua#Dev1&19');

class db {
	private $conexao = NULL;
	private $query = NULL;
	private $sql = '';
	private $data = [ ];
	private $rows = 0;
	private $result = NULL;
	private $lastId = NULL;
	private $error  = "";
	private $results = [];
	
	function __construct($result = 'object') {
		//require_once '/var/www/config.inc';
		$this->result = $result;
		$this->conexao = mysqli_connect ( SERVER_MYSQL, MYSQL_USER, MYSQL_PASS );
		$this->error = mysqli_error();
	}
	function __destruct() {
		if (is_object($this->query))
			if (get_class($this->query) == 'mysqli_result') 
			   mysqli_free_result ( $this->query );
		mysqli_close ( $this->conexao );
	}
	private function objectData() {
		if ($this->result == 'object')
			return (( object ) $this->data);
		if ($this->result == 'json')
			return (json_encode ( $this->data, JSON_UNESCAPED_UNICODE ));
		return ($this->data);
	}
	private function utf8() {
		mysqli_query ( $this->conexao, "SET NAMES UTF8" );
	}
	public function query($sql) {
		$this->sql = $sql;
		$this->utf8 ();
		$this->query = mysqli_query ( $this->conexao, $this->sql );
		$this->rows  = mysqli_affected_rows($this->conexao);
		$this->lastId = mysqli_insert_id($this->conexao);
		$this->error = mysqli_error();
	}
	public function multiQuery($sql) {
		$this->sql = $sql;
		$this->utf8 ();
		$this->rows = 0;
		$this->results = [];
		$this->query = mysqli_multi_query ( $this->conexao, $this->sql );
		if ($this->query) {
		do {
			/* store first result set */
			if ($result = mysqli_store_result($this->conexao)) {
				while ($row = $result->fetch_row()) {
					$this->rows += 1;
					$this->results[] = $row[0];
				}
				$result->free();
			}
		} while (mysqli_next_result($this->conexao));
		}
		$this->error = mysqli_error();
	}
	public function showResults() {
		return $this->results;
	}
	public function fetch() {
		$this->data = mysqli_fetch_object ( $this->query );
		$this->rows = mysqli_num_rows ( $this->query );
		$this->error = mysqli_error();
		return ($this->objectData ());
	}
	public function numRows() {
		return ($this->rows);
	}
	public function lastError() {
		return ($this->error);
	}
	public function lastSQL() {
		return ($this->sql);
	}
	public function lastId() {
		return($this->lastId);
	}
	public function fetchAll() {
		$this->data = [ ];
		$this->rows = 0;
		while ( $row = mysqli_fetch_object ( $this->query ) ) {
			array_push ( $this->data, $row );
			$this->rows ++;
		}
		return ($this->objectData ());
	}
}

?>