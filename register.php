<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<title>Portal Hágua Net - 1.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/index.css">
<link rel="icon" type="image/png" sizes="96x96" href="favicon.png">
<script src="js/register1.0.js"></script>

</head>

<body>

	<div class="modal fade" id="register-modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true"
		style="display: none;">
		<div class="modal-dialog">
			<div class="loginmodal-container">
				<h1>Registrar Nova Conta</h1>
				<br>
				<form id="register" action="ajax/saveAccount.php" method="post">
					<input type="text" name="username" placeholder="Nome Completo" id="username">
					<input type="text" name="user" placeholder="E-mail Usuário">
					<input type="password" name="pass" placeholder="Informe uma senha">
					<input type="password" name="pass-again" placeholder="Repita a senha"> 
						<hr>
						<input type="text" name="mac" placeholder="MAC do dispositivo">
						<input type="text" name="uid" placeholder="UID do dispositivo">
						<input type="text" name="apelido" placeholder="Apelido do dispositivo">
						<span class="text" style="color:#aaa;"><small>Exemplo: Casa do João</small></span>
						<hr>
						<input type="submit"
						name="login" class="login loginmodal-submit" value="Registrar">
				</form>
				<div class="login-help">
					<a href="/portal" id="close-register">Cancelar</a>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
