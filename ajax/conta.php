<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];
$db = new db ();
$db->query ( "SELECT * FROM subscriptions.hs_usuarios 
             WHERE id=$user_id" );
$data = $db->fetch();
?>
<div class="row">
	<div class="col-sm-12">
		<h1>Conta</h1>
		<form method="post" id="contaForm" class="shadow p-3 offset-lg-2 col-lg-6">
		    <?php if (isset($_POST["retorno"]) && ($_POST["retorno"]!="")) {?>
		    <div class="alert alert-warning">
                <?=$_POST["retorno"];?>
		    </div>
		    <?php }?>
			<div class="form-group">
				<label>Nome Completo</label> <input type="text" class="form-control"
					id="nome" value="<?=$data->nome?>">
			</div>
			<div class="form-group">
				<label>Email</label> <input type="email" class="form-control"
					id="email"  value="<?=$data->email?>">
			</div>
			<div class="form-group">
				<label>Nº Telegram</label> <input type="text" class="form-control"
					id="telegram"  value="<?=$data->telegram?>">
			</div>
			<hr class="bg-primary mt-4">
			<div class="form-group">
				<label>Senha Atual</label> <input type="password"
					class="form-control" id="pwdcheck" value="">
			</div>
			<div class="form-group">
				<label>Nova Senha</label> <input type="password"
					class="form-control" id="newpwd" value=""> 
			</div>
			<div class="form-group">
				<label>Repita Senha</label> <input type="password"
					class="form-control" id="newpwd_again" value="">
			</div>
			<button type="submit" class="btn btn-primary col-lg-12">Atualizar</button>
		</form>

	</div>
</div>
<script src="js/conta1.0.js"></script>
