<?php
require_once '../include/header.php';

$mac = $_POST ['mac'];

$db = new db ();
$db->query ( "SELECT tb_name,dev_name FROM subscriptions.subscribes WHERE mac='$mac'" );
$data = $db->fetch ();
$database = $data->tb_name;
$dev_name = $data->dev_name;

$db->query ( "SELECT *,
                     date_format(atualizacao,'%d/%m %H:%i') as dataAtualizacao,
                     format(capacidade,'pt-BR') as capaF,
                     format(disponivel,'pt-BR') as dispF,
                     s.id as sensor_id,
                     IF(disponivel>capacidade,0,capacidade-disponivel) as resta,
                     CONCAT(disponivel,'L - ',IF(disponivel>capacidade,100,ROUND(100*disponivel/capacidade,0)),'%') as percentual
                     FROM $database.dispositivo d LEFT JOIN 
		                  $database.sensor s ON (s.dispositivo_id=d.id) LEFT JOIN 
		                  $database.reservatorio r ON (s.reservatorio_id=r.id) 
                    WHERE d.mac='$mac'" );
$data = $db->fetchAll ();

?>
<div class="row">
	<div class="col-sm-12">
		<h3>Status <?=$dev_name;?>
		<button class="btn btn-primary float-right" id="btnRetorna"
				style="margin-top: 8px;">Voltar</button>
		</h3>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-deck">
<?php
foreach ( $data as $disp ) {
	?>
<div class="shadow card col-sm-3 bg-light text-dark">
                <canvas id="myChart"></canvas>
                <input type="hidden" id="disponivel" value="<?=$disp->disponivel;?>">
                <input type="hidden" id="resta" value="<?=$disp->resta;?>">
                <input type="hidden" id="percentual" value="<?=$disp->percentual;?>">
				<div class="card-body">
				    <div class="table-responsive" id="details">
					<h5 class="card-title"><?=$disp->apelido;?></h5>
					<table class="table">
						<tr class="sensor-row">
							<td><strong>Tipo</strong></td>
							<td><?=$disp->tipo;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>MAC</strong></td>
							<td><?=$disp->mac;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Atualização</strong></td>
							<td><?=$disp->dataAtualizacao;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Medida</strong></td>
							<td><?=$disp->medida;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Disponível</strong></td>
							<td><?=$disp->dispF;?> L</td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Reservatório</strong></td>
							<td><?=$disp->identificacao;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Formato</strong></td>
							<td><?=$disp->formato;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Medidas (AxLxC)</strong></td>
							<td><?=$disp->altura;?>x<?=$disp->largura;?>x<?=$disp->comprimento;?></td>
						</tr>
						<tr class="sensor-row">
							<td><strong>Capacidade</strong></td>
							<td><?=$disp->capaF;?> L</td>
						</tr>
					</table>
					</div>
				</div>
			</div>
<?php } ?>
		</div>
	</div>
</div>
<script>

function donut(){
	var ctx = document.getElementById('myChart').getContext('2d');
	var disponivel = $("#disponivel").val();
	var resta = $("#resta").val();
	var percentual = $("#percentual").val();
	var myDoughnutChart = new Chart(ctx, {
	    type: 'doughnut',
	    data : {
	    	    datasets: [{
	    	        data: [disponivel,resta],
	    	        backgroundColor : [ 'rgba(54, 162, 235, 1.0)',
	    	        	'rgba(132, 132, 132, 0.6)' ]
	    	    }],
	    	    labels: [
					'Disponível',
					'Utilizado'
				]

	    	},
	    options: {
			responsive: true,
			animation: {
				animateScale: true,
				animateRotate: true
			},
			circumference : Math.PI,
			rotation : Math.PI,
	        title: {
	            display: true,
	            position : 'bottom',
	            fontSize : 36,
	            padding: 0,
	            text: percentual
	        }
		}
	});
}

$(document).ready(function () {
    donut();
	$("button").click(function() {
		if (this.id=="btnRetorna") 
			getDivContent('inicial',$('#main'));
	});
	$("#details").hide();
	$("#myChart").on("click",function() {
		$("#details").toggle(1000);
	});
});
</script>