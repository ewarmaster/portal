<?php
require_once '../include/header.php';

$user_id = $_SESSION ['user_id'];

$db = new db ();
$db->query ( "SELECT s.tb_name FROM subscriptions.hs_usuarios_dispositivos d
		LEFT JOIN subscriptions.subscribes s ON (d.mac=s.mac)
		WHERE d.usuario=$user_id LIMIT 1" );
$data = $db->fetch ();
$dbname = $data->tb_name;


$db->query ( "SELECT * FROM $dbname.tipo_evento ORDER BY descricao" );
$data = $db->fetchAll ();
?>
<label>Evento</label>
<select id="evento" class="custom-select">
	<option value="*">QUALQUER EVENTO</option>
<?php
foreach ( $data as $linha ) {
	?>
	<option value="<?=$linha->id;?>">
	<?=$linha->descricao;?></option>
<?php
}
?>
</select>
<script>
$("#evento").change(function() {
	updateEventList();
});
</script>